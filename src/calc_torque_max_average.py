#!/usr/bin/python
import os
from typing import List, Union

basedir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  #
import pandas as pd
from math import pi
import matplotlib.pyplot as plt
import numpy as np
import argparse

# Mapping from motor numbers to joints
JOINTS = {
    0: 'LADPF',
    1: 'LHAA',
    2: 'LHFE',
    3: 'LKFE',
    4: 'RADPF',
    5: 'RHAA',
    6: 'RHFE',
    7: 'RKFE'
}

TRANSLATIONAL_JOINTS = [JOINTS[0], JOINTS[1], JOINTS[4], JOINTS[5]]
ROTATIONAL_JOINTS = [JOINTS[2], JOINTS[3], JOINTS[6], JOINTS[7]]


# Hardware parameters
# Dimensions
LEVERARM_ADPF = 71E-3
LEAD_ADPF = 2E-3
LEVERARM_HAA = 65E-3
LEAD_HAA = 2E-3

# Transmission ratio's
HFEKFERATIO = 100
ADPFRATIO = (2 * np.pi) * LEVERARM_ADPF / LEAD_ADPF
HAARATIO = (2 * np.pi) * LEVERARM_HAA / LEAD_HAA

# Time constants
MINTOSEC = 60
SAMPLING_TIME = 0.004

GRAPHSIZE = 20
COLUMNLENGTH = 4 #amount of columns that need to be evaluated and plotted, defined in get_output_columns

# Motor constants
# Assumed linear relation between current and torque
# https://technosoftmotion.com/wp-content/uploads/2019/10/P091.064.EtherCAT.iPOS_.UM_.pdf
# Velocity constants for MVIc joints can be found on: https://confluence.projectmarch.nl:8443/display/51/Design+choices
kv = np.array([355, 150])  # velocity constant (translational, rotational joint)
kt = 60 / (2 * pi * kv)  # torque constant

#Encoder resolution
# Encoder specifications of MVIc can be found: https://confluence.projectmarch.nl:8443/display/41TECH/Encoders+Overview
absolute_res = np.array([2 ** 13, 2 ** 17])  # resolution of encoder (translational, rotational joint)
incremental_res = np.array([2 ** 12, 2 ** 12])  # encoder resolution translational, rotational joint)


class CalcTorque(object):
    """
    The goal is to get an overview of the joint and motor torque and velocity, during different gaits.
    This will plot all torques and velocities per gait,
    Create a summary figure containing all joint and motor data per gait,
    and will store, per gait, the max, median, average and min, of the velocity and torque for both motor and joint.
    total_values.xlsx contains the max and min for every motor and joint for the whole dataset.
    """

    def __init__(self, filename_gait: str, filename_imc_states: str, output_excel: bool, output_summary: bool,
                 output_figures: bool, output_loadcases: bool):
        # Configuration
        if not output_excel and not output_summary and not output_figures and not output_loadcases:
            raise RuntimeError("At least one of the output configuration boolean should be true.")

        self.output_excel = output_excel
        self.output_summary = output_summary
        self.output_figures = output_figures
        self.output_loadcases = output_loadcases

        # Paths
        self.ftrdir = os.path.join(basedir, "ftr/")
        self.filepath_gait = os.path.join(self.ftrdir, filename_gait)
        self.file = filename_gait.replace('.ftr', '')
        self.filepath_imc_states = os.path.join(self.ftrdir, filename_imc_states)

        # Dataframes
        self.gait_data_raw = pd.read_feather(self.filepath_gait)
        self.imc_states_raw = pd.read_feather(self.filepath_imc_states)

        # Obtain relevant data
        print("getting gait data")
        gait_data = self.get_gait()
        print("getting imc data")
        imc_states = self.get_imc_data()
        print("merging data frames")
        joint_data = self.merge_data_frames(gait_data, imc_states)

        # Plot the data
        self.analyze_gait(joint_data)

    def get_gait(self) -> pd.DataFrame:
        """
        Obtain the (sub)gait status with the timestamp, so the gaits can be matched to the correct imc_data later
        :return: Relevant data (timestamp, gait, subgait)
        """
        gait_data = self.gait_data_raw[["header.stamp.secs", "gait", "subgait"]]
        return gait_data

    def executed_gaits(self, data_column: pd.DataFrame) -> np.array:
        """
        Obtain the names of occurring gaits, to see which gaits are triggered in logfile.
        :param data_column: column to analyse for unique gaits
        :return: Array of gait names
        """
        array_gaits = pd.unique(data_column)
        return array_gaits

    @staticmethod
    def add_column(data_frame: pd.DataFrame, name: str, values: Union[pd.Series, List]) -> pd.DataFrame:
        """
        Add a new column called name containing values to data_frame
        :param data_frame: Data frame to add column to
        :param name: title of new column
        :param values: Values to add in column
        :return: data_frame with added column
        """
        return data_frame.assign(**{name: values})

    def torque(self, data_frame: pd.DataFrame, ratio: float) -> pd.DataFrame:
        """
        Calculate torque by multiplying values from data_frame with a constant ratio (kt or transmission ratio)
        :param data_frame: data frame containing either current or motor torque
        :param ratio: torque constant or transmission ratio
        :return: torque value
        """
        torque = data_frame * ratio
        return torque

    def velocity(self, data_frame: pd.DataFrame, resolution: float) -> pd.DataFrame:
        """
        Calculate velocity by dividing values from data_frame with encoder resolution
        :param data_frame: data frame containing either absolute or incremental encoder values
        :param resolution: encoder resolution
        :return: angular velocity value in rpm
        """
        velocity = data_frame * MINTOSEC / resolution
        return velocity



    def add_translational_joint_data(self, imc_joint_data: pd.DataFrame, motor: int) -> pd.DataFrame:
        """
        Calculate velocity and torque of both the motor and joint for translational joints
        :param imc_joint_data: data frame containing joint data
        :param motor: Number of the motor
        :return: imc_joint_data with added columns for motor & joint velocity and torque
        """
        imc_joint_data = self.add_column(imc_joint_data, f"motor_torque_{motor}",
                                         self.torque(imc_joint_data[f"motor_current_{motor}"], kt[0]))

        if JOINTS[motor] in [JOINTS[0], JOINTS[4]]:
            imc_joint_data = self.add_column(imc_joint_data, f"joint_torque_{motor}",
                                             self.torque(imc_joint_data[f"motor_torque_{motor}"], ADPFRATIO))
        else:
            imc_joint_data = self.add_column(imc_joint_data, f"joint_torque_{motor}",
                                             self.torque(imc_joint_data[f"motor_torque_{motor}"], HAARATIO))

        imc_joint_data = self.add_column(imc_joint_data, f"motor_velocity_{motor}",
                                         self.velocity(imc_joint_data[f"incremental_velocity_{motor}"],
                                                       incremental_res[0]))

        imc_joint_data = self.add_column(imc_joint_data, f"absolute_velocity_joint_{motor}",
                                         self.velocity(imc_joint_data[f"absolute_velocity_{motor}"], absolute_res[0]))

        return imc_joint_data

    def add_rotational_joint_data(self, imc_joint_data: pd.DataFrame, motor: int) -> pd.DataFrame:
        """
        Calculate velocity and torque of both the motor and joint for rotational joints
        :param imc_joint_data: data frame containing joint data
        :param motor: Number of the motor
        :return: imc_joint_data with added columns for motor & joint velocity and torque
        """
        imc_joint_data = self.add_column(imc_joint_data, f"motor_torque_{motor}",
                                         self.torque(imc_joint_data[f"motor_current_{motor}"], kt[1]))

        imc_joint_data = self.add_column(imc_joint_data, f"motor_velocity_{motor}",
                                         self.velocity(imc_joint_data[f"incremental_velocity_{motor}"],
                                                       incremental_res[1]))

        imc_joint_data = self.add_column(imc_joint_data, f"joint_torque_{motor}",
                                         self.torque(imc_joint_data[f"motor_torque_{motor}"], HFEKFERATIO))

        imc_joint_data = self.add_column(imc_joint_data, f"absolute_velocity_joint_{motor}",
                                         self.velocity(imc_joint_data[f"absolute_velocity_{motor}"], absolute_res[1]))

        return imc_joint_data

    def get_statistics(self, array: pd.Series) -> List[float]:
        """
        Compile list of statistics for a specific column
        :param array: column to find statistics for
        :return: List with max, min, mean and median values for a column
        """
        array = pd.to_numeric(array, errors='coerce')
        return [self.get_max(array), self.get_min(array), self.get_abs_mean(array), self.get_abs_median(array)]

    @staticmethod
    def get_min(array: pd.Series) -> float:
        return min(array)

    @staticmethod
    def get_max(array: pd.Series) -> float:
        return max(array)

    @staticmethod
    def get_abs_mean(array: pd.Series) -> float:
        return abs(array).mean()

    @staticmethod
    def get_abs_median(array: pd.Series) -> float:
        return abs(array).median()

    def get_imc_data(self) -> pd.DataFrame:
        """
        Obtain relevant data from the IMC, with header.seq and timestamp, so that the data can be matched with correct
        gait. Calculate torque and velocity values for each column. Calculate statistics for each column
        :return: data_frame containing torques and velocities for all motors and joints
        """
        data_result = pd.DataFrame(index=['max', 'min', 'average', 'median'])

        # Select required columns
        imc_states = self.imc_states_raw[["header.seq", "header.stamp.secs", "header.stamp.nsecs"]]

        for motor, joint_name in JOINTS.items():
            imc_joint_data = self.imc_states_raw[["header.seq", f"motor_current_{motor}",
                                                                   f"incremental_velocity_{motor}",
                                                                   f"motor_voltage_{motor}",
                                                                   f"incremental_position_{motor}",
                                                                   f"absolute_position_{motor}",
                                                                   f"absolute_velocity_{motor}"]]

            # Add four new columns, with torque (Nm) and angular velocity (RPM) for motor and joint
            if joint_name in TRANSLATIONAL_JOINTS:
                imc_joint_data = self.add_translational_joint_data(imc_joint_data, motor)
            else:
                imc_joint_data = self.add_rotational_joint_data(imc_joint_data, motor)

            # Determine max & min from full log
            data_result = self.add_column(data_result, f"motor_torque_{motor}",
                                          self.get_statistics(imc_joint_data[f"motor_torque_{motor}"]))
            data_result = self.add_column(data_result, f"motor_velocity_{motor}",
                                          self.get_statistics(imc_joint_data[f"motor_velocity_{motor}"]))
            data_result = self.add_column(data_result, f"joint_torque_{motor}",
                                          self.get_statistics(imc_joint_data[f"joint_torque_{motor}"]))
            data_result = self.add_column(data_result, f"joint_velocity_{motor}",
                                          self.get_statistics(imc_joint_data[f"absolute_velocity_joint_{motor}"]))


            # Add calculated values to data frame
            imc_states = imc_states.merge(imc_joint_data, on="header.seq")

        if self.output_excel:
            valuedir = os.path.join(basedir, 'analysis', self.file)
            os.makedirs(valuedir, exist_ok=True)
            data_result.to_excel(f'{valuedir}/total_values.xlsx')

        # Need unique timestamps
        # define time as index * sampling time(4ms), compare with stamp.nsecs to see accuracy,
        # seems to be accurate enough
        imc_states = self.add_column(imc_states, "time", self.imc_states_raw["header.seq"]*SAMPLING_TIME)

        return imc_states

    def merge_data_frames(self, gait_data: pd, imc_states: pd):
        """
        Put the data frame with the gait information and the data set with the motor/joint data together.
        The common index is header.stamp.secs
        :param gait_data: dataframe with relevant gait data + header.stamp.secs
        :param imc_states: dataframe with relevant joint/motor data + header.stamp.secs
        :return: merged dataframes
        """

        # Remove duplicate timestamps in the dataframe containing the gaits, to prevent problems with merge.
        gait_data.drop_duplicates("header.stamp.secs", keep='last')

        # Add gait to the imc dataframe, corresponding to the time index.
        joint_data = imc_states.merge(gait_data, how='left', on='header.stamp.secs')

        # Since only the command to change gait is published, the current gait is the same as the last command
        # Remaining NaN are the time stamps BEFORE a gait command was scheduled
        joint_data["gait"].fillna(method='ffill', inplace=True)
        joint_data["subgait"].fillna(method='ffill', inplace=True)

        return joint_data

    def analyze_gait(self, joint_data: pd):
        """
        Plot all data for each joint when a specific gait starts.
        Create a summary *.pdf for with all plots for every gait.
        Create value.xlsx for every gait, with min, max, median and average.
        :param joint_data: The merged dataframe, with gait, motor & joint data
        """
        print("Executed gaits = ", self.executed_gaits(joint_data["gait"]))
        list_of_dataframes = self.split_dataframe_into_gaits(joint_data)
        # Iterate for every gait
        for gait_no, gait_current in enumerate(self.executed_gaits(joint_data["gait"])):
            print("gait ", gait_no, " of ", len(list_of_dataframes))
            if type(gait_current) == str: #skip if gait == nan
                gait_data_frame = pd.DataFrame(data=list_of_dataframes[gait_no])
                gait_data_frame = gait_data_frame.drop(columns=['subgait', 'gait'])
                print("generating output for gait ", gait_current)
                self.generate_output(gait_no, gait_current, gait_data_frame)
            else:  
                print("Skipped empty gait")

    def split_dataframe_into_gaits(self, joint_data: pd.DataFrame) -> List[pd.DataFrame]:
        """
        Split dataframe into list of dataframes, based on the gait. If difference in header.seq becomes bigger than one,
        it is assumed another gait is executed between these two timepoints, therefor if the difference >1
        joint_data will be split into multiple parts representing each time a gait occurs
        :param joint_data: data frame containing all joint, motor & gait data
        :param gait_current: gait that is to be analysed
        :return: List of split data frame for specific gait.
        """
        list_of_dataframes = []
        list_of_gaits = []
        
        # Remove data where gait = NaN, which happens at the start of a training
        # Make room in the list_of_dataframes for every gait
        for gait in self.executed_gaits(joint_data["gait"]):
            list_of_dataframes.append([None])
            list_of_gaits.append(gait)
                
        # Get all data per gait and put it in the list
        for count, gait in enumerate(list_of_gaits):
            if(type(gait) == str):
                list_of_dataframes[count] = joint_data[joint_data["gait"] == gait]
        
        return list_of_dataframes

    def generate_output(self, frame: int, gait_current: str, gait_data_frame: pd.DataFrame):
        """
        Generate output: individual plots for each column, values.xslx containing statistics for each column and
        summary.pdf for each occurrence of a gait.
        Default generates all, depending on argument also only figures, summary or values.xslx can be generated.
        :param frame: occurrence of a gait
        :param gait_current: gait that is analyzed
        :param gait_data_frame: data frame containing motor & joint values for a specific occurrence of a gait
        """
        figdir = os.path.join(basedir, 'analysis', self.file, 'figures', f'{frame}_{gait_current}')
        os.makedirs(figdir, exist_ok=True)
        print("outputting to directory: ", figdir)
        if self.output_excel:
            self.generate_excel(figdir, gait_data_frame)
        if self.output_summary:
            self.generate_summary(figdir, frame, gait_current, gait_data_frame)
        if self.output_figures:
            self.generate_figures(figdir, frame, gait_current, gait_data_frame)
        if self.output_loadcases:
            self.generate_loadcases(figdir, gait_data_frame)

    @staticmethod
    def get_output_columns(motor: int) -> List[str]:
        """
        Define columns that need to be analyzed for a specific motor
        :param motor: number corresponding to a specific joint
        :return: List of column names that need to be analyzed
        """
        return [
            f"motor_torque_{motor}",
            f"motor_velocity_{motor}",
            f"joint_torque_{motor}",
            f"absolute_velocity_joint_{motor}"
        ]

    def select_row(self, data_frame, column):
        """
        Select rows that contain a maximum value for one of the output columns
        :param data_frame: full data frame containing imc, joint & gait data
         :param column: motor and variable to find max for
        :return: rows containing data of timestep containing a maximum value
        """
        select_data = data_frame.loc[(abs(data_frame[column]) == abs(data_frame[column]).max()) &
                                     (data_frame[column] != 0)]
        select_data = self.add_column(select_data, "max value", f"{column}")
        return select_data

    def generate_excel(self, figdir: str, gait_data_frame: pd.DataFrame):
        """
        Generate excel that contains min, max, average, and median for every output column.
        :param figdir: directory that values.xslx needs to be placed into.
        :param gait_data_frame: data frame containing motor & joint values for a specific occurrence of a gait
        """
        data_result_part = pd.DataFrame(index=['max', 'min', 'average', 'median'])

        for motor, joint_name in JOINTS.items():
            output_columns = self.get_output_columns(motor)

            for column in output_columns: 
                
                result = self.get_statistics(gait_data_frame[column])
                data_result_part = self.add_column(data_result_part, f"{column} {joint_name}", result)

        data_result_part.to_excel(f'{figdir}/values.xlsx')

    def generate_loadcases(self, figdir: str, gait_data_frame: pd.DataFrame):
        """
        Generate excel that loadcases for every output column.
        :param figdir: directory that values.xslx needs to be placed into.
        :param gait_data_frame: data frame containing motor & joint values for a specific occurrence of a gait
        """
        loadcase = pd.DataFrame()

        for motor, joint_name in JOINTS.items():
            output_columns = self.get_output_columns(motor)

            for column in output_columns:
                loadcase = loadcase.append(self.select_row(gait_data_frame, column), ignore_index=False)

        loadcase.to_excel(f'{figdir}/loadcases.xlsx')

    def generate_summary(self, figdir: str, frame: int, gait_current: str, gait_data_frame: pd.DataFrame):
        """
        Generate summary pdf containing all output columns subplots.
        :param figdir: directory for storage of .pdf
        :param frame: occurrence of a gait
        :param gait_current: gait that is analyzed
        :param gait_data_frame:data frame containing motor & joint values for a specific occurrence of a gait
        """
        fig, total = plt.subplots(nrows=len(JOINTS), ncols=COLUMNLENGTH, figsize=(GRAPHSIZE, GRAPHSIZE),
                                  constrained_layout=True)
        fig.suptitle(f'gait = {gait_current}, part {frame}')

        for motor, joint_name in JOINTS.items():
            output_columns = self.get_output_columns(motor)

            for index, column in enumerate(output_columns):
                self.setup_figure(total[motor][index], column, gait_data_frame, f'{column}, {joint_name}')

        fig.savefig(f'{figdir}/summary_{gait_current}_part_{frame}.png')
        plt.close(fig)

    def generate_figures(self, figdir, frame, gait_current, gait_data_frame):
        """
        Generate a separate figure for each joint and output column.
        :param figdir: directory for storage of figures
        :param frame: occurrence of a gait
        :param gait_current:  gait that is analyzed
        :param gait_data_frame: data frame containing motor & joint values for a specific occurrence of a gait
        """
        for motor, joint_name in JOINTS.items():
            output_columns = self.get_output_columns(motor)

            for column in output_columns: 
                # To save the figure, a figure variable is required
                figsingle = plt.figure()
                figsingle_axes = plt.axes()

                self.setup_figure(figsingle_axes, column, gait_data_frame,
                                  f' {joint_name}, {column}, {joint_name}, gait = {gait_current}, part {frame}')

                # Save individual
                figsingle.savefig(f"{figdir}/{joint_name}-{column}-{gait_current}-part-{frame}.png")
                plt.close(figsingle)

    def setup_figure(self, axes, column, gait_data_frame: pd.DataFrame, title: str):
        """
        Add a plot to the pyplot axes. define styling for plots
        :param axes: figure name
        :param column: Output column that is supposed to be plotted
        :param gait_data_frame: data frame containing motor & joint values for a specific occurrence of a gait
        :param title: Title of the figure
        """
        axes.plot(gait_data_frame["time"], gait_data_frame[column])
        axes.set_title(title)
        axes.set_ylabel(column)
        axes.set_xlabel("Time [s]")
        axes.grid(True)

def parse_args():
    # Command line arguments
    parser = argparse.ArgumentParser(epilog="Defaults to generating all output data except loadcases.")
    parser.add_argument("filename_gait", help="Full name of the feather file, including '.ftr'", type=str)
    parser.add_argument("filename_imc_states", help="Full name of the feather file, including '.ftr'", type=str)

    parser.add_argument("-e", "--excel", help="Whether to generate excel files.", action='store_true')
    parser.add_argument("-f", "--figures", help="Whether to generate all figures.", action='store_true')
    parser.add_argument("-s", "--summary", help="Whether to generate a summary pdf.", action='store_true')
    parser.add_argument("-l", "--loadcases", help="Whether to generate excel files with loadcases", action='store_true')

    args = parser.parse_args()

    # If all are false, default to generating every output type
    if not args.excel and not args.summary and not args.figures and not args.loadcases:
        args.excel = True
        args.summary = True
        args.figures = True
        args.loadcases = False

    return args

def main():
    args = parse_args()

    try:
        # Make plots and files
        CalcTorque(args.filename_gait, args.filename_imc_states, args.excel, args.summary, args.figures, args.loadcases)
    except FileNotFoundError:
        print(f'Cannot find file: {args.filename_imc_states} or {args.filename_gait}')

if __name__ == '__main__':
    main()
