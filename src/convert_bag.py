#!/usr/bin/python3
import os
from pathlib import Path
import argparse
from zipfile import ZipFile

basedir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
from bagreader2 import bagreader2
import pandas as pd

TOPICS_TO_SKIP = ['/camera/foot_locations_marker_array',
'/camera/hull_marker_list',
'/camera/preprocessed_cloud',
'/camera/region_cloud',
'/diagnostics',
'/rosout',
'/rosout_agg',
'/tf',
'/tf_static']

TOPICS_TO_CONVERT = ['/march/controller/trajectory/follow_joint_trajectory/feedback', 
'/march/gait_selection/current_gait',
'/march/motor_controller_states',
'/march/pdb_data']

class ConvertBag(object):

    def __init__(self):
        pass

    def read_bag(self, bagfile: str, csv, ftr, zip, all):
        """
        :param bagfile: name of the rosbag file
        :param store: TRUE will store the topics in .ftr files
        :return: returns a list with data_frames corresponding to the topics
        """
        if ftr:
            stem = Path(bagfile).stem
            topicdir = os.path.join(basedir, 'ftr', stem)
            os.makedirs(topicdir, exist_ok=True)

        reader = bagreader2(bagfile)
        data_frame = [None] * len(reader.topics) 
        
        # Initiate zipfile object
        if zip:
            zipfolder = os.path.join(Path(bagfile).parent, 'zipfiles')
            if not(os.path.isdir(zipfolder)):
                os.mkdir(zipfolder)
            zipfile = os.path.join(zipfolder, Path(bagfile).name[:-4] + '.zip')
            print('Creating zipfile:', zipfile)
            if os.path.isfile(zipfile):
                print(zipfile, 'already exists, overwriting')
                os.remove(zipfile)
            self.zipObj = ZipFile(zipfile, 'w')
        
        # Create csv of every topic
        if all:
            for num, topic in enumerate(reader.topics):
                
                print("reading topic: ", topic)
                data_frame[num] = self.read_bag_single_topic(bagfile, topic, csv, zip)
                
                # Create ftr of only the necessary topics
                if ftr and (topic in TOPICS_TO_CONVERT):
                    self.store_ftr(data_frame[num], os.path.join(stem, topic.replace("/", "_")))
        
        else:
            # Create csv of only the necessary topics
            for num, topic in enumerate(reader.topics):
                if topic in TOPICS_TO_CONVERT:
                    
                    print("reading topic: ", topic)
                    data_frame[num] = self.read_bag_single_topic(bagfile, topic, csv, zip)
                    
                    # Create ftr of only the necessary topics
                    if ftr:
                        self.store_ftr(data_frame[num], os.path.join(stem, topic.replace("/", "_")))
        
        return data_frame

    def read_bag_single_topic(self, bagfile: str, topic: str, csv: bool, zip: bool):
        """
        Read single topic from single bag file
        :param bagfile: str data/bagfile
        :param topic: str
        :return: pandas.DataFrame
        """
        b = bagreader2(bagfile)
        datafile = b.message_by_topic(f'{topic}')
        data_frame = pd.read_csv(datafile)
        if zip and not topic in TOPICS_TO_SKIP:
            self.zipObj.write(datafile, arcname=Path(datafile).name)
        if not csv:
            os.remove(datafile)
        return data_frame

    def store_ftr(self, data_frame: pd.DataFrame, filename: str):
        """
        Store pandas.DataFrame to data/filename.ftr
        :param data_frame: pandas.DataFrame
        :param filename: str
        :return:
        """
        self.ftrdir = os.path.join(basedir, "ftr")
        os.makedirs(self.ftrdir, exist_ok=True)

        data_frame.reset_index(drop=True, inplace=True)
        data_frame.to_feather(os.path.join(self.ftrdir, f'{filename}.ftr'))

def parse_args():
    # Command line arguments
    parser = argparse.ArgumentParser(epilog="Defaults to generating csv, ftr and zipfiles")
    parser.add_argument("bagfile", help="Full name of the rosbag file", type=str)
    
    parser.add_argument("-c", "--csv", help="Whether to generate csv files.", action='store_true')
    parser.add_argument("-f", "--ftr", help="Whether to generate feather files.", action='store_true')
    parser.add_argument("-z", "--zip", help="Whether to generate a zip of csv files.", action='store_true')
    parser.add_argument("-a", "--all", help="Store all topics as csv instead of small selection.", action='store_true')

    args = parser.parse_args()

    # If all are false, default to generating every output type
    if not args.csv and not args.ftr and not args.zip:
        args.csv = True
        args.ftr = True
        args.zip = True

    return args    

def main():
    
    args = parse_args()

    if len(args.bagfile) < 2:
        print('Please provide the name of the rosbag file')
        return
 
    ConvertBag().read_bag(args.bagfile, args.csv, args.ftr, args.zip, args.all)

if __name__ == '__main__':
    main()
