import os
basedir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
import pandas as pd
import argparse


class CalculateRMSE(object):

    def __init__(self, filename: str):
        # Paths
        self.ftrdir = os.path.join(basedir, "ftr/")
        self.filepath = os.path.join(self.ftrdir, filename)
        self.data_frame = pd.read_feather(self.filepath)

        self.filebase = filename.replace('.ftr', '')

        # Extract the names of all the joints
        self.joint_names = self.data_frame.loc[0, 'feedback.joint_names']
        self.joint_names = self.joint_names[1:-1]
        self.joint_names = self.joint_names.split(",")

    def calc_rmse(self, store:bool):
        # Get erros
        df_erros_pos = self.get_error_pos()
        df_erros_vel = self.get_error_vel()

        # Get RMSE
        df_rmse_pos = self.get_rmse(df_erros_pos)
        df_rmse_vel = self.get_rmse(df_erros_vel)

        if store:
            self.store(df_rmse_pos)
            self.store(df_rmse_vel)
        else:
            print(df_rmse_pos)
            print(df_rmse_vel)

    @staticmethod
    def string_to_float(data_frame: pd.DataFrame) -> pd.DataFrame:
        data_frame = data_frame.str[1:-1]  # Remove brackets from string
        data_frame = data_frame.str.split(",", expand=True)  # Split string to individual joints
        data_frame = data_frame.astype('float')  # Convert string to float
        return data_frame

    def get_error_pos(self) -> pd.DataFrame:
        """
        The data is stored in a single string, where all 8 values are contained in one pair of brackets. These brackets
        are removed, and the one string are converted to 8 floats.
        :return: A DataFrame with the position error as floats.
        """
        df_error_pos = self.data_frame.loc[:, 'feedback.error.positions']
        df_error_pos = self.string_to_float(df_error_pos)
        df_error_pos.columns = self.joint_names  # Assign joint names to the columns of the DataFrame
        df_error_pos.name = "positionError"  # The name is used when the RMSE is stored
        return df_error_pos

    def get_error_vel(self) -> pd.DataFrame:
        """
        The data is stored in a single string, where all 8 values are contained in one pair of brackets. These brackets
        are removed, and the one string are converted to 8 floats.
        :return: A DataFrame with the velocity error as floats.
        """
        df_error_vel = self.data_frame.loc[:, 'feedback.error.velocities']
        df_error_vel = self.string_to_float(df_error_vel)
        df_error_vel.columns = self.joint_names  # Assign joint names to the columns of the DataFrame
        df_error_vel.name = "velocityError"  # The name is used when the RMSE is stored
        return df_error_vel

    @staticmethod
    def get_rmse(data_frame: pd.DataFrame) -> pd.DataFrame:
        """
        The Root-Mean_Squared-Error (RMSE) is the the error at every individual time step sqaured, and then the root of
        the mean of these squared values.
        """
        df_rmse = (data_frame ** 2).mean() ** 0.5
        df_rmse.name = data_frame.name
        return df_rmse

    def store(self, data_frame: pd.DataFrame):
        txtdir = os.path.join(basedir, 'analysis', self.filebase, 'rmse')
        os.makedirs(txtdir, exist_ok=True)
        with open(os.path.join(txtdir, f'RMSE_{data_frame.name}.txt'), 'w') as outfile:
            data_frame.to_string(outfile)
        print("Stored in directory: ", txtdir)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="Full name of the feather file, including '.ftr'", type=str)
    parser.add_argument("-s", "--store", help="If provided, will save the computations to a text file, "
                                              "if ommited the RMSE will be printed to the console",
                        action="store_true")
    args = parser.parse_args()
    try:
        # Caluclate the root mean squared error
        CalculateRMSE(args.filename).calc_rmse(args.store)
    except FileNotFoundError:
        print(f'Cannot find file: {args.filename}')


if __name__ == '__main__':
    main()