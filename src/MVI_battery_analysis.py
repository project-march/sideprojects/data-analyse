#!/usr/bin/python

import os
import pandas as pd
import matplotlib.pyplot as plt
import argparse


BASEDIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) #base folder of the files
#maximum allowed deviation in data analysis iterations, from the previous iteration.
TEMPERATURE_MAX_DELTA = 5 #temperature
VOLTAGE_MAX_DELTA = 1 #voltage
SOC_MAX_DELTA = 2 #state of charge
CURRENT_MAX_DELTA = 10 #temperature


class power_data:
    def __init__(self):
        self.ftr_dir = os.path.join(BASEDIR, "ftr/")
        self.filepath = False
        self.data_frame = False
        self.data = False

    def set_file(self, filename: str):
        self.filepath = os.path.join(self.ftr_dir, filename)
        if not os.path.isfile(self.filepath):
            raise Exception("file: ", filename, " does not exist in ", self.filepath)
        self.data = True
        self.data_frame = pd.read_feather(self.filepath)

    def calculate_watt_hour(self, plot = False, printdata = False):
        Wh = 0
        if self.data:
            elapsedTime = 0
            WhArray = []
            WhPlotArray = []
            lastCorrectVoltageIndex = 0
            lastCorrectCurrentIndex = 0
            for i in range(1, len(self.data_frame["battery_state.voltage"])):
                timeDiff = self.data_frame["header.stamp.secs"][i] - self.data_frame["header.stamp.secs"][i - 1]
                if timeDiff > 0:
                    elapsedTime += 1
                    current = self.data_frame["hv_state.total_current"][i]
                    voltage = self.data_frame["battery_state.voltage"][i]
                    allowLatestVoltageIndex = True
                    if pd.isnull(voltage):
                        voltage = self.data_frame["battery_state.voltage"][lastCorrectVoltageIndex]
                        allowLatestVoltageIndex = False
                    if (
                            voltage > (self.data_frame["battery_state.voltage"][lastCorrectVoltageIndex] + VOLTAGE_MAX_DELTA) or
                            voltage < (self.data_frame["battery_state.voltage"][lastCorrectVoltageIndex] - VOLTAGE_MAX_DELTA)
                    ):
                        allowLatestVoltageIndex = False
                        voltage = self.data_frame["battery_state.voltage"][lastCorrectVoltageIndex]
                    if allowLatestVoltageIndex:
                        lastCorrectVoltageIndex = i

                    allowLatestCurrentIndex = True
                    #Filter for PDB current, not used now due to current sensor mailfunction
                    # if pd.isnull(current):
                    #     current = self.data_frame["hv_state.total_current"][lastCorrectCurrentIndex]
                    #     allowLatestCurrentIndex = False
                    # if (
                    #         current > (
                    #         self.data_frame["hv_state.total_current"][lastCorrectCurrentIndex] + currentMaxDelta) or
                    #         current < (
                    #         self.data_frame["hv_state.total_current"][lastCorrectCurrentIndex] - currentMaxDelta)
                    # ):
                    #     allowLatestCurrentIndex = False
                    #     current = self.data_frame["hv_state.total_current"][lastCorrectCurrentIndex]
                    current = self.data_frame["hv_state.hv1_current"][i]
                    current = current + self.data_frame["hv_state.hv2_current"][i]
                    current = current + self.data_frame["hv_state.hv3_current"][i]
                    current = current + self.data_frame["hv_state.hv4_current"][i]
                    if allowLatestCurrentIndex:
                        lastCorrectCurrentIndex = i

                    intermediateWh = (current * voltage / 3600)
                    Wh = Wh + intermediateWh
                    WhArray.append(intermediateWh)
                    WhPlotArray.append(Wh)
            if plot:
                plt.plot(WhArray)
                plt.plot(WhPlotArray)
                plt.show()
            if printdata:
                print(f"Elapsed Time: {(elapsedTime / 60)} minutes")
                print(f"Used capacity: {Wh} WattHour")
        return Wh

    def plot_data(self, plot_index = "", delta = 1, graph_title = "plot", y_axis = "y axis", filter = True):
        if self.data:
            data_battery = self.data_frame[plot_index].copy()
            lastCorrectIndex = 0
            array = []
            for i in range(1, len(data_battery)):
                timeDiff = self.data_frame["header.stamp.secs"][i] - self.data_frame["header.stamp.secs"][
                    lastCorrectIndex]
                if timeDiff > 0: # or timeDiff != 0 or timeDiff == 0: #uncomment previous to see all
                    allowLatestIndex = True
                    if filter:
                        if pd.isnull(data_battery[i]):
                            data_battery[i] = data_battery[lastCorrectIndex]
                            allowLatestIndex = False
                        if (
                                data_battery[i] < (data_battery[lastCorrectIndex] - delta) or
                                data_battery[i] > (data_battery[lastCorrectIndex] + delta)
                        ):
                            data_battery[i] = data_battery[lastCorrectIndex]
                            allowLatestIndex = False
                    array.append(data_battery[i])
                    if allowLatestIndex:
                        lastCorrectIndex = i
            plt.plot(array, '-.')
            plt.ylabel(y_axis)
            plt.title(graph_title)
            plt.xlabel("Time [s]")
            plt.show()

    def plot_soc(self):
        self.plot_data("battery_state.percentage", SOC_MAX_DELTA, "State of Charge", "Percentage [%]")

    def plot_temperatures(self):
        self.plot_data("battery_state.temperature", TEMPERATURE_MAX_DELTA, "Battery avg. temperature", "Temperature [deg.]")

    def plot_voltage(self):
        self.plot_data("battery_state.voltage", VOLTAGE_MAX_DELTA, "Voltage", "Voltage [V]")

    def plot_current(self):
        self.plot_data("hv_state.total_current", CURRENT_MAX_DELTA, "Total Battery Current", "Current [A]")

    def plot_pdb_current(self):
        self.plot_data("pdb_current", CURRENT_MAX_DELTA, "PDB Current", "Current [A]")

    def plot_lv1_current(self):
        self.plot_data("lv_state.lv2_current", CURRENT_MAX_DELTA, "LV2 Current", "Current [A]", False)

    def show_colums(self):
        print(list(self.data_frame.columns))

def parse_args():
    # Command line arguments
    parser = argparse.ArgumentParser()
    
    parser.add_argument("filename", help="Full name of the feather file, including the date folder and '.ftr'", type=str)
    parser.add_argument("-w","--watthour", help="whether to plot watt hour", action='store_true')
    parser.add_argument("-t", "--temp", help="whether to plot temperatures", action='store_true')
    parser.add_argument("-s", "--soc", help="whether to plot state of charge", action='store_true')
    parser.add_argument("-p", "--pdb", help="whether to plot pdb current", action='store_true')
    parser.add_argument("-v", "--voltage", help="whether to plot voltage", action='store_true')
    parser.add_argument("-c", "--current", help="whether to plot current", action='store_true')
    
    args = parser.parse_args()

    return args

def main():
    args = parse_args()

    try:
        data = power_data()
        data.set_file(args.filename)
        #data.show_colums()
        data.calculate_watt_hour(args.watthour, True)
        if(args.temp):
            data.plot_temperatures()
        if(args.soc):
            data.plot_soc()
        if(args.pdb):
            data.plot_pdb_current()
        if(args.voltage):
            data.plot_voltage()
        if(args.current):
            data.plot_current()
    except FileNotFoundError:
        print(f'Cannot find file: {args.filename}')


if __name__ == '__main__':
    main()