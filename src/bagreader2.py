#!/usr/bin/env python
# coding: utf-8

# Author : Rahul Bhadani
# Initial Date: March 2, 2020
# About: bagreader class to read  ros bagfile and extract relevant data
# License: MIT License

#   Permission is hereby granted, free of charge, to any person obtaining
#   a copy of this software and associated documentation files
#   (the "Software"), to deal in the Software without restriction, including
#   without limitation the rights to use, copy, modify, merge, publish,
#   distribute, sublicense, and/or sell copies of the Software, and to
#   permit persons to whom the Software is furnished to do so, subject
#   to the following conditions:

#   The above copyright notice and this permission notice shall be
#   included in all copies or substantial portions of the Software.

#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
#   ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
#   TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
#   PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
#   SHALL THE AUTHORS, COPYRIGHT HOLDERS OR ARIZONA BOARD OF REGENTS
#   BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
#   AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
#   OR OTHER DEALINGS IN THE SOFTWARE.

__author__ = 'Rahul Bhadani'
__email__  = 'rahulbhadani@email.arizona.edu'
__version__ = "0.0.0" # this is set to actual version later

import sys
import ntpath
import os
import csv

import rosbag

import pandas as pd

from pathlib import Path



class bagreader2:
    '''
    `bagreader` class provides API to read rosbag files in an effective easy manner with significant hassle.
    This class is reimplementation of its MATLAB equivalent that can be found at https://github.com/jmscslgroup/ROSBagReader

    Parameters
    ----------------
    bagfile: `string`
        Bagreader constructor takes name of a bag file as an  argument. name of the bag file can be provided as the full  qualified path, relative path or just the file name.

    verbose: `bool`
        If True, prints some relevant information. Default: `True`
    
    tmp: `bool`
        If True, creates directory in /tmp folder. Default: `False`

    Attributes
    --------------
    bagfile: `string`
        Full path of the bag  file, e.g `/home/ece446/2019-08-21-22-00-00.bag`

    filename: `string`
        Name of the bag file, e.g. `2019-08-21-22-00-00.bag`
    
    dir: `string`
        Directory where bag file is located
    
    reader: `rosbag.Bag`
        rosbag.Bag object that 

    topic: `pandas dataframe`
        stores the available topic from bag file being read as a table
    
    n_messages: `integer`
        stores the number of messages
    
    message_types:`list`, `string`
        stores all the available message types
    
    datafolder: `string`
        stores the path/folder where bag file is present - may be relative to the bag file or full-qualified path.

    topic_table: `pandas.DataFrame`
        A pandas DataFrame showing list of topics, their types, frequencies and message counts

        E.g. If bag file is at `/home/ece446/2019-08-21-22-00-00.bag`, then datafolder is `/home/ece446/2019-08-21-22-00-00/`

    message_dictionary: `dictionary`
        message_dictionary will be a python dictionary to keep track of what datafile have been generated mapped by types

    Example
    ---------
    >>> b = bagreader('2020-03-01-23-52-11.bag') 

    '''

    def __init__(self , bagfile , verbose=True):
        self.bagfile = bagfile
        
        slashindices = find(bagfile, '/')
        
        # length of slashindices list will be zero if a user has pass only bag file name , e.g. 2020-03-04-12-22-42.bag
        if  len(slashindices) > 0:
            self.filename =bagfile[slashindices[-1]:]
            self.dir = bagfile[slashindices[0]:slashindices[-1]]
        else:
            self.filename = bagfile
            self.dir = './'

        self.reader = rosbag.Bag(self.bagfile)

        info = self.reader.get_type_and_topic_info() 
        self.topic_tuple = info.topics.values()
        self.topics = info.topics.keys()

        self.message_types = []
        for t1 in self.topic_tuple: self.message_types.append(t1.msg_type)

        self.n_messages = []
        for t1 in self.topic_tuple: self.n_messages.append(t1.message_count)

        self.frequency = []
        for t1 in self.topic_tuple: self.frequency.append(t1.frequency)

        self.topic_table = pd.DataFrame(list(zip(self.topics, self.message_types, self.n_messages, self.frequency)), columns=['Topics', 'Types', 'Message Count', 'Frequency'])

        self.start_time = self.reader.get_start_time()
        self.end_time = self.reader.get_end_time()

        self.datafolder = bagfile[0:-4]

        self.verbose = verbose

        if not os.path.exists(self.datafolder):
            try:
                original_umask = os.umask(0)
                print('changing permissions for ', Path(self.datafolder).parent)
                os.chmod(Path(self.datafolder).parent, 511)
                os.mkdir(self.datafolder)
            except OSError:
                print("[ERROR] Failed to create the data folder {0}.".format(self.datafolder))
            else:
                if self.verbose:
                    print("[INFO]  Successfully created the data folder {0}.".format(self.datafolder))
            finally:
                os.umask(original_umask)

    def message_by_topic(self, topic):
        '''
        Class method `message_by_topic` to extract message from the ROS Bag by topic name `topic`

        Parameters
        ---------------
        topic: `str`
            
            Topic from which to extract messages.
        Returns
        ---------
        `str`
            The name of the csv file to which data from the `topic` has been extracted

        Example
        -----------
        >>> b = bagreader('/home/ivory/CyverseData/ProjectSparkle/sparkle_n_1_update_rate_100.0_max_update_rate_100.0_time_step_0.01_logtime_30.0_2020-03-01-23-52-11.bag') 
        >>> msg_file = b.message_by_topic(topic='/catvehicle/vel')

        '''

        msg_list = []
        tstart =None
        tend = None
        time = []
        for topic, msg, t in self.reader.read_messages(topics=topic, start_time=tstart, end_time=tend): 
            time.append(t)
            msg_list.append(msg)

        msgs = msg_list

        if len(msgs) == 0:
            print("No data on the topic:{}".format(topic))
            return None

        # set column names from the slots
        cols = ["Time"]
        m0 = msgs[0]
        slots = m0.__slots__
        for s in slots:
            v, s = slotvalues(m0, s)
            if isinstance(v, tuple):
                snew_array = [] 
                p = list(range(0, len(v)))
                snew_array = [s + "_" + str(pelem) for pelem in p]
                s = snew_array
            
            if isinstance(s, list):
                for i, s1 in enumerate(s):
                    cols.append(s1)
            else:
                cols.append(s)
        
        tempfile = self.datafolder + "/" + topic.replace("/", "-") + ".csv"
        file_to_write = ntpath.dirname(tempfile) + '/' + ntpath.basename(tempfile)[1:]

        if sys.hexversion >= 0x3000000:
            opencall = open(file_to_write, "w", newline='')
        else:
            opencall = open(file_to_write, 'wb')

        with opencall as f:
            writer = csv.writer(f, delimiter=',')
            writer.writerow(cols) # write the header
            for i, m in enumerate(msgs):
                slots = m.__slots__
                vals = []
                vals.append(time[i].secs + time[i].nsecs*1e-9)
                for s in slots:
                    v, s = slotvalues(m, s)
                    if isinstance(v, tuple):
                        snew_array = [] 
                        p = list(range(0, len(v)))
                        snew_array = [s + "_" + str(pelem) for pelem in p]
                        s = snew_array

                    if isinstance(s, list):
                        for i, s1 in enumerate(s):
                            vals.append(v[i])
                    else:
                        vals.append(v)
                writer.writerow(vals)
        return file_to_write

def slotvalues(m, slot):
    vals = getattr(m, slot)
    try:
        slots = vals.__slots__
        varray = []
        sarray = []
        for s in slots:
            vnew, snew = slotvalues(vals, s)       
            if isinstance(snew, list):
                for i, snn in enumerate(snew):
                    sarray.append(slot + '.' + snn)
                    varray.append(vnew[i])
            elif isinstance(snew, str):
                sarray.append(slot + '.' + snew)
                varray.append(vnew)    
                
        return varray, sarray
    except AttributeError:
        return vals, slot
        
def find(s, ch):
    '''
    Function `find` returns indices all the occurence of `ch` in `s` 

    Parameters
    -------------
    s: `string`
        String or a setence where to search for occurrences of the character `ch`

    s: `char`
        Character to look for

    Returns
    ---------
    `list`
        List of indices of occurrences of character `ch` in the string `s`.

    '''
    return [i for i, ltr in enumerate(s) if ltr == ch]


