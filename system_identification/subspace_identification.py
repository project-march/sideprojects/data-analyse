import pandas as pd
import os
import colorama
import numpy as np
from scipy import linalg
from scipy import signal
from numpy.lib.stride_tricks import as_strided
import matplotlib.pyplot as plt


basedir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
np.set_printoptions(precision=2)
colorama.init(autoreset=True)
YELLOW = "\u001b[33m"
GREEN = "\u001b[32m"
BLUE= "\u001b[34m"


class SubspaceID(object):

    def __init__(self, joint_states_file: str, current_state_file: str):

        # Paths
        self.ftrdir = os.path.join(basedir, "ftr/")
        self.filepath_joint_states = os.path.join(self.ftrdir, joint_states_file)
        self.filepath_current_state = os.path.join(self.ftrdir, current_state_file)
        self.filepath_store = os.path.dirname(joint_states_file)

        # Read out data, convert to DataFrame
        self.joint_states = pd.read_feather(self.filepath_joint_states)
        self.gait_states = pd.read_feather(self.filepath_current_state)

        # Parameters
        # self.sampling_time = 0.020  # sampling time in s
        self.state_space = {}  # Empty dict for state space matrices
        self.state_space['dt'] = 0.02  # Sampling time in seconds

        # Define attributes
        self.input_column_names = []
        self.output_column_names = []
        self.states = 0
        self.hankel_rows = 0
        self.input_hankel_s = 0
        self.input_hankel_0 = 0
        self.output_hankel_s = 0
        self.output_hankel_0 = 0
        self.input_states = 0
        self.output_states = 0
        self.preprocessor()
        self.subspace_identification()

    # Prepocessing Methods
    def preprocessor(self):
        self.crop_joint_states()
        self.joint_states['position_0'].plot()
        self.set_time()

    def ask_start_gait(self) -> float:
        """
        Ask user to which data the model should be fitted
        :return: The timestamp of the first datapoint
        """
        print(f'{YELLOW}The following gaits were performed:')
        print(f'{YELLOW}{self.gait_states["state"].to_string()}')
        start_gait_index = int(input(f"{GREEN}What is the starting gait? Please provide the index number."))
        print(f'{BLUE}Chosen starting gait: {self.gait_states.state.iloc[start_gait_index]}')
        return self.gait_states.Time.iloc[start_gait_index]

    def ask_end_gait(self) -> float:
        """
        Ask the user to which data the model should be fitted
        :return: The timestamp of the latest datapoint
        """
        # The end time of the sequence, will be the start time of the gait after the last gait
        end_gait_index = int(input(f"{GREEN}What was the last performed gait? Please provide the index number.")) + 1

        print(f'{BLUE}Chosen end gait: {self.gait_states.state.iloc[end_gait_index - 1]}')

        # If the last gait is part of the sequence, the end time is the last value (index -1)
        if end_gait_index >= len(self.gait_states.index):
            end_gait_index = -1
        return self.gait_states.Time.iloc[end_gait_index]

    def crop_joint_states(self):
        """
        Remove not used data points from dataset
        """
        # Obtain start and end gait
        start_gait_time = self.ask_start_gait()
        end_gait_time = self.ask_end_gait()

        # Cut the rows
        self.joint_states.drop(self.joint_states[self.joint_states.Time < start_gait_time].index, inplace=True)
        self.joint_states.drop(self.joint_states[self.joint_states.Time > end_gait_time].index, inplace=True)
        # self.joint_states.drop(self.joint_states[self.joint_states.index < 1000].index, inplace=True)
        # self.joint_states.drop(self.joint_states[self.joint_states.index > 1005].index, inplace=True)

        # Cut the columns
        self.joint_states = self.joint_states.filter(['Time', 'position_0', 'velocity_0', 'effort_0'])

    def set_time(self):
        """
        Convert the timestamps to datetime data type, and mark it as it index. Subspace ID requires evenly spaced data
        in time.
        """
        self.joint_states.Time = pd.to_datetime(self.joint_states.Time, unit='s')
        self.joint_states = self.joint_states.set_index(self.joint_states.Time)
        self.joint_states = self.joint_states.drop(['Time'], axis=1)

        # Resample to fixed time deltas
        oidx = self.joint_states.index  # old index
        nidx = pd.date_range(oidx.min(), oidx.max(), freq=f'{self.state_space["dt"] * 1000}ms')  # new index
        self.joint_states = self.joint_states.reindex(oidx.union(nidx)).interpolate('index').reindex(nidx, copy=False)
        print(f'{YELLOW}Number of measurements: {len(self.joint_states.index)}')

    # Subspace Identification methods
    def subspace_identification(self):
        self.ask_states()
        self.construct_hankel()
        state_sequence = self.state_sequence()
        self.system_estimation(state_sequence)
        self.plot_model(state_sequence)
        self.save_system()

    def ask_states(self):
        """
        Ask user how many inputs and outputs the system has.
        """
        self.input_states = int(input(f'{GREEN}How many inputs ("u" in literature)?'))
        self.output_states = int(input(f'{GREEN}How many outputs ("y" in literature)?'))
        self.hankel_rows = int(input(f'{GREEN}How many input & output vectors should be stacked to form '
                                     f'the Hankel matrices (s in literature)'))

    def ask_system_states(self, singular_values) -> int:
        """
        Ask user how many states (dynamic order) the fitted model should have
        """
        # Plot shows an estimation of the order of the dynamics
        plt.subplot(2, 1, 1)
        plt.plot(singular_values, 'o')
        plt.yscale('log')
        plt.show()
        return int(input(f"{GREEN}How many states does the sytem have "
                         f"(number of singular values that are larger than the rest)?"))

    def construct_hankel(self):
        """
        Hankel matrices consist of stacked input and output vectors, and are used to fit a model to the data.
        These matrices tend to be very large, but contain duplicate data
        """
        # shorter names fo readabilty
        u = self.input_states
        y = self.output_states
        s = self.hankel_rows

        # If the number of columns is too large, QR factorisation can become too demanding
        hankel_columns = np.minimum(len(self.joint_states) - (2 * s - 1), 2000) #  Hankel Columns
        self.state_space['N'] = hankel_columns

        # Extract inputs and outputs from pd.DataFrame to ndarray
        outputs = self.joint_states['position_0'].to_numpy().reshape(-1, y, 1)
        inputs = self.joint_states[['effort_0']].to_numpy().reshape(-1, u, 1)

        # Construct the matrices without saving the duplicate values to memory
        o0, o1, o2 = outputs.strides
        i0, i1, i2 = inputs.strides
        self.output_hankel_0 = as_strided(outputs, (s, y, hankel_columns, 1), (o0, o1, o0, o2)).\
            reshape(y * s, hankel_columns)

        self.output_hankel_s = as_strided(outputs[s:-1, :, :], (s, y, hankel_columns, 1), (o0, o1, o0, o2)).\
            reshape(y * s, hankel_columns)

        self.input_hankel_0 = as_strided(inputs, (s, u, hankel_columns, 1), (i0, i1, i0, i2))\
            .reshape(u * s, hankel_columns)

        self.input_hankel_s = as_strided(inputs[s:-1, :, :], (s, u, hankel_columns, 1), (i0, i1, i0, i2))\
            .reshape(u * s, hankel_columns)

    def state_sequence(self) -> np.array:
        """
        Obtain a estimation of the state sequence using Numeric Algorithm for Subspace Identification (N4SID).
        N4SID is a method to estimate the state space sequence of the Kalman Filter
        A state space model (and Kalman Filter) can be fitted to this state sequence
        """
        # Shorter names fo readabilty
        u = self.input_states
        y = self.output_states
        s = self.hankel_rows

        # N4SID
        # Create required matrices for N4SID
        instrumental_variable_matrix = np.vstack((self.input_hankel_0, self.output_hankel_0))
        rq = np.vstack((self.input_hankel_s, instrumental_variable_matrix, self.output_hankel_s))

        # QR factorisation and extract required spaces
        rq = rq.transpose()
        r = linalg.qr(rq, mode='r')[0]  # Returns r as upper triangular matrix
        r = r.transpose()  # N4SID requires lower triangular matrix
        r22 = r[s*u:s*(2*u+y), s*u:s*(2*u+y)]
        r32 = r[s*(2*u+y):-1, s*u:s*(2*u+y)]

        # Approximate row space using singular value decomposition
        intermediate_matrix = np.matmul(r32, np.linalg.inv(r22))
        intermediate_matrix = np.matmul(intermediate_matrix, instrumental_variable_matrix)
        u, singular_values, v = np.linalg.svd(intermediate_matrix)

        # Singular values of the rows space indicate dynamics order
        self.states = self.ask_system_states(singular_values)

        # Row space from lower singular values can be seen as noise on the data
        singular_values = np.diag(singular_values[0:self.states])
        v = v[:, 0:self.states]

        # Obtain state space sequence of the Kalman Filter
        state_sequence = np.matmul(linalg.sqrtm(singular_values), v.transpose())
        return state_sequence

    def system_estimation(self, state_sequence: np.array):
        """
        Uses the state sequence of the Kalman filter to obtain the state matrices, using a Linear Least Square problem
        """
        # s = self.hankel_rows
        # LSQ problem: min_{system_matrices} ( x1 - system_matrices * x0)^2
        x0 = np.vstack((state_sequence[:, 0:-2], self.input_hankel_s[0:self.input_states, 0:-2]))
        x1 = np.vstack((state_sequence[:, 1:-1], self.output_hankel_s[0:self.output_states, 0:-2]))

        # np.linal.requires transposed version of problem described above
        system_matrices = np.linalg.lstsq(x0.transpose(), x1.transpose(), rcond=None)[0]
        system_matrices = system_matrices.transpose()

        # Extract system matrices
        self.state_space['A'] = system_matrices[0:self.states, 0:self.states]
        self.state_space['B'] = system_matrices[0:self.states, self.states:self.states+self.input_states]
        self.state_space['C'] = system_matrices[self.states:self.states+self.output_states, 0: self.states]
        self.state_space['D'] = system_matrices[self.states:self.states+self.output_states, self.states:self.states+self.input_states]

        self.estimate_kalman_gain(system_matrices, x0, x1)
        # system = list((A, B, C, D, self.sampling_time))
        # system = StateSpace(A, B, C, D, self.sampling_time)
        # return system

    def estimate_kalman_gain(self, system_matrices: np.array, x0: np.array, x1: np.array):
        residuals = x1 - system_matrices @ x0
        noise_covariance = 1 / self.state_space['N'] * residuals @ residuals.transpose()
        Q = noise_covariance[0:self.states, 0:self.states]
        S = noise_covariance[0:self.states, self.states:self.states+self.output_states]
        R = noise_covariance[self.states:self.states+self.output_states, self.states:self.states+self.output_states]
        P = linalg.solve_discrete_are(self.state_space['A'].transpose(), self.state_space['C'].transpose(), Q, R, s=S)
        K = (S + self.state_space['A'] @ P.transpose() @ self.state_space['C'].transpose()) @ \
                                np.linalg.inv(R + self.state_space['C'] @ P.transpose() @
                                              self.state_space['C'].transpose())
        self.state_space['K'] = K
        print(Q, R)

    def plot_model(self, state_sequence: np.array):
        """
        Create a plot to compare the model output to the measurements
        """
        # Create StateSpace object for easy simulation. Requires dt in [s] instead of [ms]
        # sys = signal.StateSpace(self.state_space['A'] - self.state_space['K'] @ self.state_space['C'],
        #                         self.state_space['B'], self.state_space['C'],
        #                         self.state_space['D'], dt=self.state_space['dt'])

        # Expose model to same input the real system
        u = self.joint_states.effort_0.to_numpy()
        y_measured = self.joint_states['position_0'].to_numpy()

        sim_time = 501
        x_hat = np.zeros((self.states, 1, sim_time))
        y_hat = np.zeros((np.size(self.state_space['C'], 0), 1, sim_time))
        for k in range(500):
            x_hat[:, :, k+1] = (self.state_space['A'] - self.state_space['K'] @ self.state_space['C']) @ x_hat[:, :, k]\
                               + self.state_space['B'] * u[k] + self.state_space['K'] * y_measured[k]
            y_hat[:, :, k] = self.state_space['C'] @ x_hat[:, :, k] + self.state_space['D'] * u[k]

        # Create time array
        self.joint_states['time'] = self.joint_states.index.to_frame()
        self.joint_states['delta_t'] = (self.joint_states['time'] - self.joint_states['time'].iloc[0])
        t = self.joint_states['delta_t'].dt.total_seconds().to_numpy()
        t.astype("timedelta64[s]").astype(float)

        # Initial condition and simulation
        x0 = state_sequence[:, 0]
        x = np.zeros((self.states, 1, sim_time))
        y = np.zeros((np.size(self.state_space['C'], 0), 1, sim_time))
        for k in range(500):
            x[:, :, k+1] = self.state_space['A'] @ x[:, :, k] + self.state_space['B'] * u[k]
            y[:, :, k] = self.state_space['C'] @ x[:, :, k] + self.state_space['D'] * u[k]

        plt.figure()
        plt.plot(t[0:500], y_measured[0:500], 'b')
        plt.plot(t[0:500], y[0, 0, 0:500], 'r')
        plt.plot(t[0:500], u[0:500]*0.1, 'g')
        plt.show()

    def save_system(self):
        store_dir = os.path.join(basedir, 'analysis', self.filepath_store)
        os.makedirs(store_dir, exist_ok=True)
        for key, value in self.state_space.items():
            path = os.path.join(store_dir, f'system_{key}')
            if isinstance(value, np.ndarray):
                np.savetxt(path, value)
            else:
                try:
                    file = open(os.path.join(store_dir, f"{key}"), "w")
                    file.writelines(f'{value}')
                    file.close()
                except:
                    print(f"Could not save value with key: {key}")


SubspaceID("4kg_1minute/_march_joint_states.ftr", "4kg_1minute/_march_gait_selection_current_state.ftr")

