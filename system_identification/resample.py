import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
import argparse
basedir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class Resample(object):
    def __init__(self, joint_states: str, sent_effort: str, dt: float):
        self.dt = dt

        # Paths
        self.ftrdir = os.path.join(basedir, "ftr/")
        self.filepath_joint_states = os.path.join(self.ftrdir, joint_states)
        self.filepath_sent_effort = os.path.join(self.ftrdir, sent_effort)
        self.filepath_store = os.path.dirname(joint_states)

        # Read out data, convert to DataFrame
        self.joint_states = pd.read_feather(self.filepath_joint_states)
        self.sent_effort = pd.read_feather(self.filepath_sent_effort)

        self.joint_states['effort_0'].plot()
        self.sent_effort['data_0'].plot()
        plt.show()

        print(len(self.joint_states.index))
        print(len(self.sent_effort.index))

        self.add_pos()
        self.set_time()
        self.resample()
        self.remove_columns()
        self.add_time()
        self.save()


    def set_time(self):
        """
        Convert the timestamps to datetime data type, and mark it as it index. Subspace ID requires evenly spaced data
        in time.
        """
        self.joint_states.Time = pd.to_datetime(self.joint_states.Time, unit='s')
        self.joint_states = self.joint_states.set_index(self.joint_states.Time)

        self.sent_effort.Time = pd.to_datetime(self.sent_effort.Time, unit='s')
        self.sent_effort = self.sent_effort.set_index(self.sent_effort.Time)

        print((self.joint_states.index[0] - self.sent_effort.index[0]).total_seconds())

    def resample(self):
        oidx = self.joint_states.index  # old index
        nidx = pd.date_range(oidx.min(), oidx.max(), freq=f'{self.dt * 1000}ms')  # new index
        self.joint_states = self.joint_states.reindex(oidx.union(nidx)).interpolate('index').reindex(nidx, copy=False)

        oidx = self.sent_effort.index  # old index
        nidx = pd.date_range(oidx.min(), oidx.max(), freq=f'{self.dt * 1000}ms')  # new index
        self.sent_effort = self.sent_effort.reindex(oidx.union(nidx)).interpolate('index').reindex(nidx, copy=False)

    def remove_columns(self):
        self.joint_states = self.joint_states.filter(['position_0', 'effort_0'])
        self.sent_effort = self.sent_effort.filter(items=['position_0', 'data_0'])

    def add_time(self):
        self.joint_states.insert(0, 'Time', self.joint_states.index.to_frame())
        self.joint_states['Time'] = (self.joint_states['Time'] - self.joint_states['Time'].iloc[0])
        self.joint_states['Time'] = self.joint_states['Time'].dt.total_seconds().to_numpy()

        self.sent_effort.insert(0, 'Time', self.sent_effort.index.to_frame())
        self.sent_effort['Time'] = (self.sent_effort['Time'] - self.sent_effort['Time'].iloc[0])
        self.sent_effort['Time'] = self.sent_effort['Time'].dt.total_seconds().to_numpy()

    def add_pos(self):
        self.sent_effort.insert(1, 'position_0', self.joint_states['position_0'])

    def save(self):
        store_dir = os.path.join(basedir, 'analysis', self.filepath_store)
        os.makedirs(store_dir, exist_ok=True)
        np.savetxt(os.path.join(store_dir, 'measured_effort.txt'), self.joint_states.values)
        np.savetxt(os.path.join(store_dir, 'sent_effort.txt'), self.sent_effort.values)



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("joint_states", help="Full name of the feather file, including '.ftr'", type=str)
    parser.add_argument("sent_effort", help="Full name of the feather file, including '.ftr'", type=str)
    parser.add_argument("dt", help="Sampling time in seconds", type=float)

    args = parser.parse_args()
    try:
        # Caluclate the capacity
        Resample(args.joint_states, args.sent_effort, args.dt)
    except FileNotFoundError:
        print(f'Cannot find file: {args.filename}')


if __name__ == '__main__':
    main()
