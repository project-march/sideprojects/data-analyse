# MARCH Data Analyse
This repository contains all data analytics code made by Project MARCH

### Installation
To install the required packages:
```
pip3 install -r requirements.txt
```

### Example Usage
Recommend to run from the "data-analyse" directory. All files that require an extracted rosbag, use a `*.ftr`
data set. Files like `src/calc_battery_capacity` require an input file path. This path should be relative from `/ftr`.

#### convert_bag
````
cd ~/data-analyse/
python3 ./src/convert_bag.py "<path/to/log.bag>"
````
The "*.bag" file does not necessarily need to be in the "/data/" directory,
but the "*.ftr" files will be stored in the "/ftr/filename/" directory 
The "*.csv" files will be stored in /data/filename directory
The "*.zip" files will be stored in /data/zipfiles directory

Currently used (and thus converted) topics are:
/march/controller/trajectory/follow_joint_trajectory/feedback for calc_rmse.py
/march/gait_selection/current_gait for calc_torque_max_average.py
/march/motor_controller_states for calc_torque_max_average.py and calc_battery_capacity.py
/march/pdb_data for MVI_battery_analysis.py

Optional arguments:
* `-c --csv`    	store csv files for above topics
* `-f --ftr`   	    store ftr files for above topics
* `-z --zip`	    store the csv files in a zipfile
* `-a --all`	    store a csv file for every topic instead of the selection mentioned above

#### Analyse the battery usage from a training session
After creating a .ftr file of a rosbag with pdb data, battery analysis can be executed by:
```
cd ~/data-analyse/
python3 ./src/MVI_battery_analysis.py "<path/to/file.ftr>"
```

Optional arguments:
* `-w --watthour`	plot watt hour
* `-t --temp`   	plot temperatures
* `-s --soc`	    plot state of charge
* `-p --pdb`    	plot pdb current
* `-v --voltage`	plot voltage
* `-c --current`	plot current

#### Calculate the battery capacity of a imc_states topic
After creating a .ftr file, the capacity per motor can be computed by:
```
cd ~/data-analyse/
python3 ./src/calc_battery_capacity.py "<path/to/_march_motor_controller_states.ftr>" 
```
or, if you want to save and store the results, give the optional
argument `--store` or `-s`
```
python3 ./src/calc_battery_capacity.py "<path/to/_march_motor_controller_states.ftr>" --store
```
The capacities will be saved in `"analysis/filename/logs"`  

#### Calculate max/min average and median torque/velocity per gait
After creating a .ftr file, motor/joint information can be computed and plotted by:
```
cd ~/march_ws/data-analyse/
python3 src/calc_torque_max_average.py "jointData/_march_gait_selection_current_gait.ftr" "jointData/_march_motor_controller_states.ftr" 
```
where `"jointData/_march_gait_selection_current_gait.ftr"` and `"jointData/_march_motor_controller_states.ftr" ` can be any `*.ftr` file with the joint data that needs to be analyzed. These files should include current/voltage, encoder, timestamp and gait data.
By default, this file will create the following for each gait:
* plots for motor torque and speed
* plots for joint torque and speed
* excel sheets with max, min and mean values
* a summary figure per gait
* an excel sheet with loadcases, located in the `"analysis/filename/logs"`. 

Optional commands:
* `-e --excel` 		generate Min, Max, Mean excel sheets
* `-f --figures`	generate individual plot figures
* `-s --summary`	generate summary figure per gait
* `-l --loadcases`	generate excel sheets with loadcases


Note that this script can take a few minutes, depending on the size of the datasets used.

#### Calculate the RMSE from the controller feedback topic
After creating a .ftr file, the RMSE (root mean squared error) per joint can be computed by:
```
cd ~/data-analyse
python3 ./src/calc_rmse.py "<path/to/_march_controller_trajectory_follow_joint_trajectory_feedback.ftr>"
```
Add `-s` or `--store` to store the RMSE to a `*.txt` file.
