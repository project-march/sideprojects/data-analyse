from bokeh.models import AjaxDataSource
from bokeh.plotting import figure, show, gridplot
import argparse

NUMBER_OF_TYPES = 3


class MpcDashboard(object):
    def __init__(self, host_port: str, number_of_joints: int):
        self.source_estimation = AjaxDataSource(
            data_url=f"http://{host_port}/estimation",
            polling_interval=50,
            mode="replace",
        )
        self.source_measurement = AjaxDataSource(
            data_url=f"http://{host_port}/measurement",
            polling_interval=50,
            mode="append",
            max_size=200,
        )
        figures = []
        for joint_number in range(number_of_joints):
            for type_it in range(NUMBER_OF_TYPES):
                figures.append(
                    self.create_figure(joint_number=joint_number, type_int=type_it)
                )

        grid = gridplot(figures, ncols=3, merge_tools=False)
        show(grid)

    @staticmethod
    def get_type(argument) -> str:
        type_map = {0: "position", 1: "velocity", 2: "input"}
        return type_map.get(argument, "Invalid type")

    @staticmethod
    def get_joint_name(joint_number) -> str:
        joint_name_map = {
            # 0: "Left ankle",
            0: "Left hip AA",
            # 2: "Left hip FE",
            # 3: "Left knee",
            # 4: "Right ankle",
            1: "Right hip AA",
            # 6: "Right hip FE",p
            # 7: "Right knee",
        }
        return joint_name_map.get(joint_number, "Invalid joint number")

    def create_figure(self, joint_number: int, type_int: int) -> figure:
        joint_name = self.get_joint_name(joint_number)
        type_str = self.get_type(type_int)
        fig = figure(title=f"{joint_name}: {type_str}")
        # Measured values
        fig.line(
            x="time",
            y=f"joint_{joint_number}_{type_str}",
            source=self.source_measurement,
            color="blue",
            legend_label=f"measured {type_str}",
        )
        fig.circle_dot(
            x="time",
            y=f"joint_{joint_number}_{type_str}",
            source=self.source_measurement,
            color="blue",
            legend_label=f"measured {type_str}",
            size=6,
        )
        # Estimated values
        fig.line(
            x="time",
            y=f"joint_{joint_number}_estimation_{type_str}",
            source=self.source_estimation,
            color="red",
            legend_label=f"estimated {type_str}",
        )
        fig.circle_dot(
            x="time",
            y=f"joint_{joint_number}_estimation_{type_str}",
            source=self.source_estimation,
            color="red",
            line_color="red",
            legend_label=f"estimated {type_str}",
            size=6,
        )
        # Reference values
        fig.line(
            x="time",
            y=f"joint_{joint_number}_reference_{type_str}",
            source=self.source_measurement,
            color="green",
            legend_label=f"reference {type_str}",
        )
        fig.circle_dot(
            x="time",
            y=f"joint_{joint_number}_reference_{type_str}",
            source=self.source_measurement,
            color="green",
            legend_label=f"reference {type_str}",
            size=6,
        )
        fig.line(
            x="time",
            y=f"joint_{joint_number}_reference_{type_str}",
            source=self.source_estimation,
            color="green",
            legend_label=f"reference {type_str}",
        )
        fig.circle_dot(
            x="time",
            y=f"joint_{joint_number}_reference_{type_str}",
            source=self.source_estimation,
            color="green",
            line_color="green",
            legend_label=f"reference {type_str}",
            size=6,
        )

        fig.legend.location = "top_left"

        fig.x_range.follow = "end"
        fig.x_range.follow_interval = 1
        fig.x_range.range_padding = 0.1
        fig.x_range.range_padding_units = "absolute"
        return fig


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("host", help="Host where the data is obtained from", type=str)
    parser.add_argument("port", help="Port where the data is obtained from", type=str)
    parser.add_argument(
        "joints",
        help="Number of joints",
        type=int,
    )
    args = parser.parse_args()
    host_port = f"{args.host}:{args.port}"
    MpcDashboard(host_port, args.joints)


if __name__ == "__main__":
    main()
