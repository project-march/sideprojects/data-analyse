import pandas as pd
from pathlib import Path
import os
import yaml
from yaml.loader import SafeLoader
import os
from bagpy import bagreader

basedir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PLUGIN_NAME = 'ROSBAG PARSER'
data_frame_to_publish = []
ALLOWED_TOPICS = ['/march/imc_states', '/march/controller/trajectory/state']
skipped_msgs = []

def read_bag_single_topic(topic: str, reader: bagreader):
    data = reader.message_by_topic(f'{topic}')
    data_frame = pd.read_csv(data)
    return data_frame


def get_topic_list(reader):

    topic_list = []
    for topic in reader.topics:
        if topic in ALLOWED_TOPICS:
            topic_list.append(topic)
    return topic_list


def string_to_float(data_frame: pd.DataFrame) -> pd.DataFrame:
    data_frame = data_frame.str[1:-1]  # Remove brackets from string
    data_frame = data_frame.str.split(",", expand=True)  # Split string to individual joints
    data_frame = data_frame.astype('float')  # Convert string to float
    return data_frame


def get_headers(file_path):
    reader = bagreader(file_path)
    topic_list = get_topic_list(reader)
    data_frame = [None] * len(topic_list)
    tmp_df = pd.DataFrame()
    global skipped_msgs

    for num, topic in enumerate(topic_list):
        data_frame[num] = read_bag_single_topic(topic, reader)
        data_frame[num].set_index('header.seq', inplace=True)

        for header in data_frame[num]:
            if data_frame[num][f'{header}'].dtype == 'object':
                try:
                    list_with_state_df = string_to_float(data_frame[num][f'{header}'])
                    for joint_number in list_with_state_df.columns:
                        tmp_df[f'{header}_{joint_number}'] = list_with_state_df[joint_number].values
                    del data_frame[num][f'{header}']

                except ValueError:
                    skipped_msgs.append(f'{header}')
                    del data_frame[num][f'{header}']

    global data_frame_to_publish
    data_frame_to_publish = tmp_df
    while len(data_frame) != 0:
        data_frame_to_publish = data_frame_to_publish.join(data_frame.pop(), rsuffix='_drop.')

    data_frame_to_publish.drop([col for col in data_frame_to_publish.columns if 'drop' in col], axis=1, inplace=True)
    data_frame_to_publish.reset_index(inplace=True)
    data_frame_to_publish = data_frame_to_publish.rename(columns={'index': 'time'})
    headers = []
    for header in data_frame_to_publish.keys():
        headers.append({'name': header, 'time_base': 1})

    return headers


def write_values(write_to_db, file_path):
    write_to_db(data_frame_to_publish.values, time_base=1)


def get_metadata(file_path):
    path = Path(file_path).parent.absolute()
    path = os.path.join(path, 'metadata.yaml')

    with open(path, 'r') as meta_data_file:
        meta_data = yaml.load(meta_data_file)

    return meta_data
