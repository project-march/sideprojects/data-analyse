import matplotlib.pyplot as plt
import numpy as np
import yaml
from matplotlib import gridspec
from scipy.fft import fft, fftfreq
from scipy.interpolate import BPoly


def main():

    # Get user input
    subgait_path = input("Supply absolute path to the subgait file: ")
    joint_name = input("Supply the name of the joint you want to perform FFT on: ")

    # Read subgait as a yaml file into a directory
    with open(subgait_path, 'r') as stream:
        try:
            subgait = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
            return 0

    # Get the joint_name directory of the subgait
    try:
        joint_dict = subgait["joints"][joint_name]
    except KeyError as e:
        print("The joint with name: " + str(e) + " doesn't exist in this subgait")
        return 0

    # Retrieve the subgait positions, velocities and time setpoints
    position = []
    velocity = []
    time = []

    num_setpoints = len(joint_dict)
    for i in range(num_setpoints):
        position.append(joint_dict[i]["position"])  # position setpoints [rad]
        velocity.append(joint_dict[i]['velocity'])  # velocity setpoints [rad/s]
        time.append(joint_dict[i]['time_from_start'] / 1e9)  # time from start [s]

    # Perform the same cubic spline as the joint_trajectory
    yi = []
    for i in range(num_setpoints):
        yi.append([position[i], velocity[i]])

    # We do a cubic spline here, like the ros joint_trajectory_action_controller,
    # https://wiki.ros.org/robot_mechanism_controllers/JointTrajectoryActionController
    interpolated_position = BPoly.from_derivatives(time, yi)
    interpolated_velocity = interpolated_position.derivative()

    # Create an interpolated data array with a sampling time of dt
    dt = 0.02  # 50 Hz
    position_data = []
    velocity_data = []
    time_data = []

    time_steps = int(time[-1] / dt)
    for i in range(time_steps):
        position_data.append(float(interpolated_position(i * dt)))
        velocity_data.append(float(interpolated_velocity(i * dt)))
        time_data.append(i * dt)

    # Perform 1-D discrete Fast Fourier Transform
    # https://docs.scipy.org/doc/scipy/reference/tutorial/fft.html

    xf = fftfreq(time_steps, dt)[:time_steps // 2]
    yf = fft(position_data)
    yf = 2.0 / time_steps * np.abs(yf[0:time_steps // 2])

    # plot the results
    fig = plt.figure()
    gs = gridspec.GridSpec(5, 5)

    # Plot interpolated subgait (position and velocity)
    plt.subplot(gs[0:2, 0:5])
    plt.title("Interpolated subgait")
    plt.plot(time_data, position_data)
    plt.plot(time_data, velocity_data)
    plt.ylabel("Amplitude")
    plt.xlabel("Time (s)")
    plt.legend(["position (rad)", "velocity (rad/s)"])
    plt.grid()

    # plot position fft
    plt.subplot(gs[3:5, 0:2])
    plt.title("FFT of the Interpolated Position")
    plt.plot(xf, yf)
    plt.ylabel("Amplitude")
    plt.xlabel("Frequency (Hz)")
    plt.grid()

    # plot position fft (log scale)
    plt.subplot(gs[3:5, 3:5])
    plt.title("FFT of the Interpolated Position")
    plt.plot(xf, yf)
    plt.ylabel("Amplitude")
    plt.xlabel("Frequency (Hz)")
    plt.xscale('log')
    plt.grid()

    plt.show()


if __name__ == "__main__":
    main()
