import yaml


# Read the subgait as a yaml file and return it as an directory
from scipy.interpolate import BPoly


def get_subgait(subgait_path):
    # open subgait file
    with open(subgait_path, 'r') as stream:
        try:
            # dump yaml stream into subgait directory
            subgait = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
            return 0

    return subgait


# write the subgait dictionary to the supplied path
def write_subgait(subgait, path):

    # Write the subgait to a file
    # This will overwrite the file's contents if the file already exists
    with open(path, 'w+') as outfile:
        yaml.dump(subgait, outfile, default_flow_style=False)

    return 0


def write_dict(dict, path):

    # Write the dict to a file
    # This will overwrite the file's contents if the file already exists
    with open(path, 'w+') as outfile:
        yaml.dump(dict, outfile, default_flow_style=False)

    return 0


def get_joint_setpoint_data(subgait_dict, joint_name, **kwargs):

    time_ratio = 1
    for key, value in kwargs.items():
        if key == "time" and value == "seconds":
            time_ratio = 1e-9

    # Retrieve the subgait positions, velocities and time setpoints
    position_data = []
    velocity_data = []
    time_from_start_data = []

    joint_dict = subgait_dict["joints"][joint_name]
    num_setpoints = len(joint_dict)
    for i in range(num_setpoints):
        position_data.append(joint_dict[i]["position"])  # position setpoints [rad]
        velocity_data.append(joint_dict[i]['velocity'])  # velocity setpoints [rad/s]
        time_from_start_data.append(joint_dict[i]['time_from_start'] * time_ratio)  # time from start

    return position_data, velocity_data, time_from_start_data


def modify_joint_setpoint_data(subgait_dict, joint_name, position_data, velocity_data, time_from_start_data, **kwargs):

    time_ratio = 1
    for key, value in kwargs.items():
        if key == "time" and value == "seconds":
            time_ratio = 1e9

    # Modify the subgait positions, velocities and time setpoints
    joint_dict = subgait_dict["joints"][joint_name]
    num_setpoints = len(joint_dict)
    for i in range(num_setpoints):
        joint_dict[i]["position"] = position_data[i]  # position setpoints [rad]
        joint_dict[i]['velocity'] = velocity_data[i]  # velocity setpoints [rad/s]
        joint_dict[i]['time_from_start'] = time_from_start_data[i] * time_ratio  # time from start

    return subgait_dict


def interpolate_subgait(subgait, joint_name, dt):

    # Retrieve the subgait positions, velocities and time setpoints
    position, velocity, time_from_start = get_joint_setpoint_data(subgait, joint_name, time="seconds")

    # # Perform the same cubic spline as the joint_trajectory
    yi = []
    num_setpoints = len(position)
    for i in range(num_setpoints):
        yi.append([position[i], velocity[i]])

    # # We do a cubic spline here, like the ros joint_trajectory_action_controller,
    # # https://wiki.ros.org/robot_mechanism_controllers/JointTrajectoryActionController
    interpolated_position = BPoly.from_derivatives(time_from_start, yi)
    interpolated_velocity = interpolated_position.derivative()

    # Create an interpolated data array with a sampling time of dt
    position_data = []
    velocity_data = []
    time_from_start_data = []

    # Start interpolation
    time_steps = int(time_from_start[-1] / dt)
    for i in range(time_steps):
        position_data.append(float(interpolated_position(i * dt)))
        velocity_data.append(float(interpolated_velocity(i * dt)))
        time_from_start_data.append(i * dt)

    return position_data, velocity_data, time_from_start_data


def get_max_values(subgait_path, verbose=False):

    properties = {}

    subgait = get_subgait(subgait_path)
    for joint in subgait["joints"].keys():

        # Check if joint already exists
        if not properties.get(joint):

            # create joint_properties dict
            joint_properties = {
                joint: {
                    "max_pos": [],
                    "max_vel": []
                }
            }

            # add to properties dict
            properties.update(joint_properties)

        # interpolate subgait
        try:
            interpolated_position, interpolated_velocity, *other = interpolate_subgait(subgait, joint, 0.02)
        except ValueError as e:
            continue

        # maximum position and velocity
        properties[joint]["max_pos"].append(float(max(interpolated_position)))
        properties[joint]["max_vel"].append(float(max(interpolated_velocity)))

    for joint in properties.keys():
        properties[joint]["max_pos"] = max(properties[joint]["max_pos"])
        properties[joint]["max_vel"] = max(properties[joint]["max_vel"])

    if verbose:
        print(properties)

    return properties
