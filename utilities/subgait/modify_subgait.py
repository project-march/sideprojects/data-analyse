import os.path
import numpy as np
import sys

import subgait_tools as sgt


# Scale the subgait in the time axis
def scale_in_time(subgait, **options):
    # Options:
    # 1) scale:      multiply the time_from_start by a scaling ratio
    # 2) end_time:   scale the subgait such that it has a duration equal to the supplied end_time

    # Get subgait duration
    subgait_duration = int(subgait["duration"])

    # Handle the given options
    if len(options) > 1:
        print("Error: Multiple time scaling methods have been given, please use only one")
        return 0

    elif options.get("scale"):
        scale = float(options.get("scale"))
        end_time_in_nanoseconds = int(subgait_duration * scale)
        scale_ratio = scale

    elif options.get("end_time"):
        end_time = int(options.get("end_time"))
        end_time_in_nanoseconds = int(end_time * 1e9)
        scale_ratio = end_time_in_nanoseconds / subgait_duration

    else:
        print("Error: No (correct) scaling methods have been given, use \"scale\" or \"end_time\"")
        return 0

    # Scale the time_from_start of all joints in the subgait
    joint_dict = subgait["joints"]
    for joint in joint_dict.keys():
        num_setpoints = len(joint_dict[joint])
        for i in range(num_setpoints - 1):
            scaled_time_from_start = int(joint_dict[joint][i]['time_from_start'] * scale_ratio)
            joint_dict[joint][i]['time_from_start'] = scaled_time_from_start

        # Make sure that the last time_from_start and subgait duration is equal to each other
        joint_dict[joint][num_setpoints - 1]['time_from_start'] = end_time_in_nanoseconds
        subgait["duration"] = end_time_in_nanoseconds

    return subgait


def scale_in_amplitude(subgait, joint_name, **options):
    # Options:
    # 1) scale:         multiply the position and velocity amplitude by a scaling ratio
    # 2) max_amplitude: scale the subgait amplitude such that it doesn't violate the max_amplitude bound

    # Handle the given options
    if len(options) > 1:
        print("Error: Multiple amplitude scaling methods have been given, please use only one")
        return 0

    elif options.get("scale"):
        scale = float(options.get("scale"))
        scale_ratio = scale

    elif options.get("max_amplitude"):
        max_amplitude = float(options.get("max_amplitude"))

        # Determine the max amplitude of the unmodified subgait by interpolation
        interpolated_position, *other = sgt.interpolate_subgait(subgait, joint_name, 0.02)
        max_pos = float(np.max(interpolated_position))
        if max_pos <= max_amplitude:
            scale_ratio = 1
        else:
            scale_ratio = max_amplitude/max_pos

    else:
        print("Error: No (correct) scaling methods have been given, use \"scale\" or \"max_amplitude\"")
        return 0

    # Get setpoint data
    positions, velocities, time_from_start = sgt.get_joint_setpoint_data(subgait, joint_name)

    # Scale the setpoint data
    num_setpoints = len(positions)
    for i in range(num_setpoints):
        positions[i] = positions[i] * scale_ratio
        velocities[i] = velocities[i] * scale_ratio
        time_from_start[i] = time_from_start[i]

    # Modify the setpoint data
    sgt.modify_joint_setpoint_data(subgait, joint_name, positions, velocities, time_from_start)

    return 0


def get_properties(subgait, subgait_path):

    properties = {
        "subgait": os.path.basename(subgait_path)
    }

    joint_dict = subgait["joints"]
    for joint in joint_dict.keys():

        # create joint_properties dict
        joint_properties = {
            joint: {
                "properties": {
                    "subgait": 0.0,
                    "max_pos": 0.0,
                    "max_vel": 0.0
                }
            }
        }

        # add to properties dict
        properties.update(joint_properties)

        # interpolate subgait
        interpolated_position, interpolated_velocity, *other = sgt.interpolate_subgait(subgait, joint, 0.02)

        # maximum position and velocity
        properties[joint]["properties"]["max_pos"] = float(max(interpolated_position))
        properties[joint]["properties"]["max_vel"] = float(max(interpolated_velocity))

    sgt.write_dict(properties, 'subgait_properties.yaml')

    return 0


def print_help_message():
    file_name = os.path.basename(__file__)

    print("%s is a tool for modifying subgaits, see options below for the possible modifications" % file_name)
    print("")
    print("Syntax: python3 %s modify_subgait.py [-h] [-f|t|st|a|sa|o]" % file_name)
    print("Options:")
    print("-h,  --help                              Print this Help message")
    print("-i,  --input                             Specify input subgait to modify")
    print("-t,  --set_end_time                      Scale the subgait duration to the specified end time [s]")
    print("-st, --scale_time_by                     Scale the subgait duration by the specified scale ratio")
    print("-a,  --set_max_amplitude <joint_name>    Scale the subgait amplitude to the specified max amplitude [rad]")
    print("-sa, --scale_amplitude_by <joint_name>   Scale the subgait amplitude by the specified scale ratio")
    print("-o,  --output                            Set the output path of the modified subgait, overwrites "
          "input subgait if not specified")
    print("")


def main():
    # Get command-line arguments
    arguments = sys.argv
    del arguments[0]

    # Define subgait path and dictionary
    subgait_path = ""
    subgait = {}

    # Exit if no arguments are given
    if len(arguments) == 0:
        print("no arguments given, use -h or --help for the help page")
        return 0

    # Handle the given arguments
    for idx, arg in enumerate(arguments):
        if arg == "-h" or arg == "--help":
            if idx == 0:
                print_help_message()
                return 0
            else:
                continue

        elif arg == "-i" or arg == "--input":

            # Get subgait
            subgait_path = arguments[idx + 1]
            subgait = sgt.get_subgait(subgait_path)

            # Clear args from arguments and continue
            del arguments[idx:idx + 1]
            continue

        elif arg == "-t" or arg == "--set_end_time":
            if not subgait:
                print("Error: Please provide a subgait")
                return 0

            # Scale the subgait in the time axis
            end_time = arguments[idx + 1]
            scale_in_time(subgait, end_time=end_time)

            # Clear args from arguments and continue
            del arguments[idx:idx + 1]
            continue

        elif arg == "-st" or arg == "--scale_time_by":
            if not subgait:
                print("Error: Please provide a subgait")
                return 0

            # Scale the subgait in the time axis
            scale = arguments[idx + 1]
            scale_in_time(subgait, scale=scale)

            # Clear args from arguments and continue
            del arguments[idx:idx + 1]
            continue

        elif arg == "-a" or arg == "--set_max_amplitude":
            if not subgait:
                print("Error: Please provide a subgait")
                return 0

            # Scale the subgait in the y axis
            max_amplitude = arguments[idx + 1]
            joint_name = arguments[idx + 2]
            if not subgait["joints"][joint_name]:
                print("Error: Joint name doesn't exist")
                return 0
            scale_in_amplitude(subgait, joint_name, max_amplitude=max_amplitude)

            # Clear args from arguments and continue
            del arguments[idx:idx + 2]
            continue

        elif arg == "-sa" or arg == "--scale_amplitude_by":
            if not subgait:
                print("Error: Please provide a subgait")
                return 0

            # Scale the subgait in the y axis
            scale = arguments[idx + 1]
            joint_name = arguments[idx + 2]
            if not subgait["joints"][joint_name]:
                print("Error: Joint name doesn't exist")
                return 0
            scale_in_amplitude(subgait, joint_name=joint_name, scale=scale)

            # Clear args from arguments and continue
            del arguments[idx:idx + 2]
            continue

        elif arg == "-p" or arg == "--properties":
            if not subgait:
                print("Error: Please provide a subgait")
                return 0

            get_properties(subgait, subgait_path)

        elif arg == "-o" or arg == "--output":
            if not subgait:
                print("Error: Please provide a subgait")
                return 0

            # Write subgait to specified path
            try:
                output_path = arguments[idx + 1]
                sgt.write_subgait(subgait, output_path)
            except IndexError:
                print("Error: No output path given")
                return 0

            # Exit the script after writing the modified subgait
            return 0

        else:
            print(arg + " is not a valid argument")
            return 0

    # Overwrite the input file
    # executes if no output argument has been given
    conformation = input("Are you sure you want to overwrite the input subgait file? (Y/n):\n")
    if subgait_path and conformation.lower() == "y":
        sgt.write_subgait(subgait, subgait_path)
    else:
        print("No subgait file has been modified")

    return 0


if __name__ == "__main__":
    main()
