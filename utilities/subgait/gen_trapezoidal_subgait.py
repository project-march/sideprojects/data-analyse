import os
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
import yaml


# Function to generate a trapezoidal signal
def trapzoid_signal(t, width=2., slope=1., amp=1., offs=0):
    a = slope * width * signal.sawtooth(2 * np.pi * t / width, width=0.5) / 4.
    a[a > amp / 2.] = amp / 2.
    a[a < -amp / 2.] = -amp / 2.

    return a + amp / 2. + offs


# Function to generate a subgait
def generate_subgait(position, velocity, time_from_start):
    joint_name = "rotational_joint"
    subgait = dict(
        description="Trapezoidal gait",
        duration=int(time_from_start[-1]),
        gait_type='',
        joints=dict()
    )
    joint = subgait["joints"][joint_name] = []

    for i in range(len(position)):
        joint.append(dict(
            position=float(position[i]),
            time_from_start=int(time_from_start[i]),
            velocity=float(velocity[i]),
        ))

    subgait["name"] = "trapezoidal"
    subgait["version"] = "trapezoidal_final"

    # Write the subgait to a file
    if not os.path.exists('output'):
        os.makedirs('output')
    with open('output/trapezoidal.subgait', 'w+') as outfile:
        yaml.dump(subgait, outfile, default_flow_style=False)

    return 0


# Define the trapezoidal parameters and get the data points
dt = 0.01
T = 6
t_nano = np.linspace(0, int(T*1e9), int(T/dt))   # nanoseconds
t = np.linspace(0, T, int(T/dt))

position = trapzoid_signal(t, width=T, slope=1, amp=0.349)
velocity = np.diff(position/dt)
velocity = np.append(velocity, [0.0])

# If desired, split the signal in half such that the signal only rises
split_signal = True
if split_signal:
    mid_idx = int(T/(2*dt))
    t_nano = t_nano[0:mid_idx]
    t = t[0:mid_idx]

    position = position[0:mid_idx]
    velocity = velocity[0:mid_idx]

# generate the subgait
generate_subgait(position, velocity, t_nano)

# plot the resulting trapezoidal signal
plt.plot(t, position)
plt.plot(t, velocity)
plt.show()
