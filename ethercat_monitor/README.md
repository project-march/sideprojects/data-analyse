# EtherCAT monitor
The following scripts can be used to monitor EtherCAT traffic.

## Prerequisites
* libcap
* pkg-config

For Linux:
```
sudo apt-get install libpcap-dev
sudo apt-get install pkg-config
```

## Building
For building the project, make sure that VSCode is configured. Once, you open the project, you'll be prompted to configure the project using a CMakeList. Choose the `CMakeList.txt` located in the `ethercat_monitor` folder.

Then open the `CMakeList.txt` file and edit the last 2 lines to match the desination of the lpcap library. To find the location of this library, use the following commands:

```
pkg-config --cflags libpcap
pkg-config --libs libpcap
```
The last step is to change the `if_name` to the correct interface name.

Now, you can build the code. Using VSCode, use the Build button at the bottom left.

## Running
After the code is build/compiled, you're ready to run the script. This can be done by following these steps:

* Navigate to the `ethercat_monitor/build` folder
* Run `./ethercat_monitor [optional: slave_number]`

The `slave_number` is an optional parameter to choose which slave's process data you want to print.