#include <pcap.h>
#include <stdio.h>
#include <stdint.h>
#include <arpa/inet.h>
#include <string.h>
#include <iostream>
#include <netinet/if_ether.h>

#define DATA_SIZE 864
#define HEADER_SIZE 26
#define PACKET_SIZE 912

int slave_num = 2;

// Define a structure to hold the process data
typedef struct __attribute__((packed)) {
    float Axis0TargetTorque;
    float Axis0TargetPosition;
    float Axis0FuzzyTorque;
    float Axis0FuzzyPosition;
    float Axis0PositionP;
    float Axis0PositionI;
    float Axis0PositionD;
    float Axis0TorqueP;
    float Axis0TorqueD;
    uint32_t Axis0RequestedState;
    float Axis1TargetTorque;
    float Axis1TargetPosition;
    float Axis1FuzzyTorque;
    float Axis1FuzzyPosition;
    float Axis1PositionP;
    float Axis1PositionI;
    float Axis1PositionD;
    float Axis1TorqueP;
    float Axis1TorqueD;
    uint32_t Axis1RequestedState;
    uint32_t ChecksumMOSI;
} ProcessDataMDriveRx;

typedef struct __attribute__((packed)) {
    uint32_t Axis0AbsolutePosition;
    float Axis0Current;
    float Axis0MotorVelocity;
    uint32_t Axis0ODriveError;
    uint32_t Axis0AxisError;
    uint64_t Axis0MotorError;
    uint32_t Axis0EncoderError;
    uint32_t Axis0TorqueSensorError;
    uint32_t Axis0ControllerError;
    uint32_t Axis0State;
    float Axis0ODriveTemperature;
    float Axis0MotorTemperature;
    int32_t Axis0ShadowCount;
    float Axis0Torque;
    uint32_t Axis1AbsolutePosition;
    float Axis1Current;
    float Axis1MotorVelocity;
    uint32_t Axis1ODriveError;
    uint32_t Axis1AxisError;
    uint64_t Axis1MotorError;
    uint32_t Axis1EncoderError;
    uint32_t Axis1TorqueSensorError;
    uint32_t Axis1ControllerError;
    uint32_t Axis1State;
    float Axis1ODriveTemperature;
    float Axis1MotorTemperature;
    int32_t Axis1ShadowCount;
    float Axis1Torque;
    uint32_t AIEAbsolutePosition;
    uint32_t ChecksumMISO;
    uint32_t ChecksumMOSIStatus;
} ProcessDataMDriveTx;

typedef struct __attribute__((packed))
{
    ProcessDataMDriveRx MDrive1Rx;
    ProcessDataMDriveRx MDrive2Rx;
    ProcessDataMDriveRx MDrive4Rx;
    ProcessDataMDriveRx MDrive5Rx;
    ProcessDataMDriveTx MDrive1Tx;
    ProcessDataMDriveTx MDrive2Tx;
    ProcessDataMDriveTx MDrive4Tx;
    ProcessDataMDriveTx MDrive5Tx;
} ProcessData;

void print_mdrive_data(ProcessDataMDriveRx &MDriveRxStruct, ProcessDataMDriveTx &MDriveTxStruct) {
    std::cout << "RxPDO:" << std::endl;
    std::cout << "  Axis0 Target Torque: " << MDriveRxStruct.Axis0TargetTorque << std::endl;
    std::cout << "  Axis0 Target Position: " << MDriveRxStruct.Axis0TargetPosition << std::endl;
    std::cout << "  Axis0 Fuzzy Torque: " << MDriveRxStruct.Axis0FuzzyTorque << std::endl;
    std::cout << "  Axis0 Fuzzy Position: " << MDriveRxStruct.Axis0FuzzyPosition << std::endl;
    std::cout << "  Axis0 Position P: " << MDriveRxStruct.Axis0PositionP << std::endl;
    std::cout << "  Axis0 Position I: " << MDriveRxStruct.Axis0PositionI << std::endl;
    std::cout << "  Axis0 Position D: " << MDriveRxStruct.Axis0PositionD << std::endl;
    std::cout << "  Axis0 Torque P: " << MDriveRxStruct.Axis0TorqueP << std::endl;
    std::cout << "  Axis0 Torque D: " << MDriveRxStruct.Axis0TorqueD << std::endl;
    std::cout << "  Axis0 Requested State: " << MDriveRxStruct.Axis0RequestedState << std::endl;
    std::cout << "  Axis1 Target Torque: " << MDriveRxStruct.Axis1TargetTorque << std::endl;
    std::cout << "  Axis1 Target Position: " << MDriveRxStruct.Axis1TargetPosition << std::endl;
    std::cout << "  Axis1 Fuzzy Torque: " << MDriveRxStruct.Axis1FuzzyTorque << std::endl;
    std::cout << "  Axis1 Fuzzy Position: " << MDriveRxStruct.Axis1FuzzyPosition << std::endl;
    std::cout << "  Axis1 Position P: " << MDriveRxStruct.Axis1PositionP << std::endl;
    std::cout << "  Axis1 Position I: " << MDriveRxStruct.Axis1PositionI << std::endl;
    std::cout << "  Axis1 Position D: " << MDriveRxStruct.Axis1PositionD << std::endl;
    std::cout << "  Axis1 Torque P: " << MDriveRxStruct.Axis1TorqueP << std::endl;
    std::cout << "  Axis1 Torque D: " << MDriveRxStruct.Axis1TorqueD << std::endl;
    std::cout << "  Axis1 Requested State: " << MDriveRxStruct.Axis1RequestedState << std::endl;
    std::cout << "  Checksum MOSI: " << MDriveRxStruct.ChecksumMOSI << std::endl;


    std::cout << "TxPDO:" << std::endl;
    std::cout << "  Axis0 Absolute Position: " << MDriveTxStruct.Axis0AbsolutePosition << std::endl;
    std::cout << "  Axis0 Current: " << MDriveTxStruct.Axis0Current << std::endl;
    std::cout << "  Axis0 Motor Velocity: " << MDriveTxStruct.Axis0MotorVelocity << std::endl;
    std::cout << "  Axis0 ODrive Error: " << MDriveTxStruct.Axis0ODriveError << std::endl;
    std::cout << "  Axis0 Axis Error: " << MDriveTxStruct.Axis0AxisError << std::endl;
    std::cout << "  Axis0 Motor Error: " << MDriveTxStruct.Axis0MotorError << std::endl;
    std::cout << "  Axis0 Encoder Error: " << MDriveTxStruct.Axis0EncoderError << std::endl;
    std::cout << "  Axis0 Torque Sensor Error: " << MDriveTxStruct.Axis0TorqueSensorError << std::endl;
    std::cout << "  Axis0 Controller Error: " << MDriveTxStruct.Axis0ControllerError << std::endl;
    std::cout << "  Axis0 State: " << MDriveTxStruct.Axis0State << std::endl;
    std::cout << "  Axis0 ODrive Temperature: " << MDriveTxStruct.Axis0ODriveTemperature << std::endl;
    std::cout << "  Axis0 Motor Temperature: " << MDriveTxStruct.Axis0MotorTemperature << std::endl;
    std::cout << "  Axis0 Shadow Count: " << MDriveTxStruct.Axis0ShadowCount << std::endl;
    std::cout << "  Axis0 Torque: " << MDriveTxStruct.Axis0Torque << std::endl;
    std::cout << "  Axis1 Absolute Position: " << MDriveTxStruct.Axis1AbsolutePosition << std::endl;
    std::cout << "  Axis1 Current: " << MDriveTxStruct.Axis1Current << std::endl;
    std::cout << "  Axis1 Motor Velocity: " << MDriveTxStruct.Axis1MotorVelocity << std::endl;
    std::cout << "  Axis1 ODrive Error: " << MDriveTxStruct.Axis1ODriveError << std::endl;
    std::cout << "  Axis1 Axis Error: " << MDriveTxStruct.Axis1AxisError << std::endl;
    std::cout << "  Axis1 Motor Error: " << MDriveTxStruct.Axis1MotorError << std::endl;
    std::cout << "  Axis1 Encoder Error: " << MDriveTxStruct.Axis1EncoderError << std::endl;
    std::cout << "  Axis1 Torque Sensor Error: " << MDriveTxStruct.Axis1TorqueSensorError << std::endl;
    std::cout << "  Axis1 Controller Error: " << MDriveTxStruct.Axis1ControllerError << std::endl;
    std::cout << "  Axis1 State: " << MDriveTxStruct.Axis1State << std::endl;
    std::cout << "  Axis1 ODrive Temperature: " << MDriveTxStruct.Axis1ODriveTemperature << std::endl;
    std::cout << "  Axis1 Motor Temperature: " << MDriveTxStruct.Axis1MotorTemperature << std::endl;
    std::cout << "  Axis1 Shadow Count: " << MDriveTxStruct.Axis1ShadowCount << std::endl;
    std::cout << "  Axis1 Torque: " << MDriveTxStruct.Axis1Torque << std::endl;
    std::cout << "  AIE Absolute Position: " << MDriveTxStruct.AIEAbsolutePosition << std::endl;
    std::cout << "  Checksum MISO: " << MDriveTxStruct.ChecksumMISO << std::endl;
    std::cout << "  Checksum MOSI Status: " << MDriveTxStruct.ChecksumMOSIStatus << std::endl;
    std::cout << std::endl;
}

void extract_data(const u_char *packet, ProcessData &data)
{
    // Assuming the EtherCAT payload starts after the Ethernet header
    const u_char *ethercat_payload = packet + HEADER_SIZE;

    // Copy the process data from the payload into the structure
    memcpy(&data, ethercat_payload, DATA_SIZE);
}

int main(int argc, char *argv[])
{
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *handle = pcap_open_offline("/Users/kimhernandezsalvador/Downloads/rkfe_small_shoot_12-06-2024_1925.pcapng", errbuf);  // Open the .pcapng file
    if (handle == NULL)
    {
        std::cerr << "Couldn't open file: " << errbuf << std::endl;
        return 2;
    }

    struct pcap_pkthdr header;
    const u_char *packet;

    while ((packet = pcap_next(handle, &header)) != NULL)
    {
        // Parse the Ethernet header
        struct ether_header *eth_header = (struct ether_header *)packet;

        // Check if the packet is an EtherCAT frame (EtherType 0x88A4)
        if (ntohs(eth_header->ether_type) == 0x88A4)
        {
            ProcessData data;

            // Extract process data from the EtherCAT frame
            if(header.len == PACKET_SIZE) {
                extract_data(packet, data);

                if((argc == 1) || (std::stoi(argv[1]) == 1)) {
                    printf("MDrive: Slave 1\n");
                    print_mdrive_data(data.MDrive1Rx, data.MDrive1Tx);
                }
                if((argc == 1) || (std::stoi(argv[1]) == 2)) {
                    printf("MDrive: Slave 2\n");
                    print_mdrive_data(data.MDrive2Rx, data.MDrive2Tx);
                }
                if((argc == 1) || (std::stoi(argv[1]) == 4)) {
                    printf("MDrive: Slave 4\n");
                    print_mdrive_data(data.MDrive4Rx, data.MDrive4Tx);
                }
                if((argc == 1) || (std::stoi(argv[1]) == 5)) {
                    printf("MDrive: Slave 5\n");
                    print_mdrive_data(data.MDrive5Rx, data.MDrive5Tx);
                }

                break;
            }
        }
    }

    // Close the pcap file
    pcap_close(handle);

    return 0;
}
