#include <pcap.h>
#include <stdio.h>
#include <stdint.h>
#include <arpa/inet.h>
#include <string.h>
#include <iostream>
#include <netinet/if_ether.h>

#define DATA_SIZE 208
#define HEADER_SIZE 26
#define PACKET_SIZE 256

// Define a structure to hold the process data
typedef struct __attribute__((packed)) {
    float Axis0TargetTorque;
    float Axis0TargetPosition;
    float Axis0FuzzyTorque;
    float Axis0FuzzyPosition;
    float Axis0PositionP;
    float Axis0PositionI;
    float Axis0PositionD;
    float Axis0TorqueP;
    float Axis0TorqueD;
    uint32_t Axis0RequestedState;
    float Axis1TargetTorque;
    float Axis1TargetPosition;
    float Axis1FuzzyTorque;
    float Axis1FuzzyPosition;
    float Axis1PositionP;
    float Axis1PositionI;
    float Axis1PositionD;
    float Axis1TorqueP;
    float Axis1TorqueD;
    uint32_t Axis1RequestedState;
//    uint32_t ChecksumMOSI;

    uint32_t Axis0AbsolutePosition;
    float Axis0Current;
    float Axis0MotorVelocity;
    uint32_t Axis0ODriveError;
    uint32_t Axis0AxisError;
    uint64_t Axis0MotorError;
    uint32_t Axis0EncoderError;
    uint32_t Axis0TorqueSensorError;
    uint32_t Axis0ControllerError;
    uint32_t Axis0State;
    float Axis0ODriveTemperature;
    float Axis0MotorTemperature;
    int32_t Axis0ShadowCount;
    float Axis0Torque;
    uint32_t Axis1AbsolutePosition;
    float Axis1Current;
    float Axis1MotorVelocity;
    uint32_t Axis1ODriveError;
    uint32_t Axis1AxisError;
    uint64_t Axis1MotorError;
    uint32_t Axis1EncoderError;
    uint32_t Axis1TorqueSensorError;
    uint32_t Axis1ControllerError;
    uint32_t Axis1State;
    float Axis1ODriveTemperature;
    float Axis1MotorTemperature;
    int32_t Axis1ShadowCount;
    float Axis1Torque;
    uint32_t AIEAbsolutePosition;
    uint32_t ChecksumMISO;
//    uint32_t ChecksumMOSIStatus;

} ProcessDataMDrive;

void extract_data(const u_char *packet, ProcessDataMDrive &data)
{
    // Assuming the EtherCAT payload starts after the Ethernet header
    const u_char *ethercat_payload = packet + HEADER_SIZE;

    // Copy the process data from the payload into the structure
    memcpy(&data, ethercat_payload, DATA_SIZE);
    std::cout << "Size of header " << sizeof(struct ether_header) << std::endl;

//     Print some of the extracted values

    std::cout << "RxPDO:" << std::endl;
    std::cout << "  Axis0 Target Torque: " << data.Axis0TargetTorque << std::endl;
    std::cout << "  Axis0 Target Position: " << data.Axis0TargetPosition << std::endl;
    std::cout << "  Axis0 Fuzzy Torque: " << data.Axis0FuzzyTorque << std::endl;
    std::cout << "  Axis0 Fuzzy Position: " << data.Axis0FuzzyPosition << std::endl;
    std::cout << "  Axis0 Position P: " << data.Axis0PositionP << std::endl;
    std::cout << "  Axis0 Position I: " << data.Axis0PositionI << std::endl;
    std::cout << "  Axis0 Position D: " << data.Axis0PositionD << std::endl;
    std::cout << "  Axis0 Torque P: " << data.Axis0TorqueP << std::endl;
    std::cout << "  Axis0 Torque D: " << data.Axis0TorqueD << std::endl;
    std::cout << "  Axis0 Requested State: " << data.Axis0RequestedState << std::endl;
    std::cout << "  Axis1 Target Torque: " << data.Axis1TargetTorque << std::endl;
    std::cout << "  Axis1 Target Position: " << data.Axis1TargetPosition << std::endl;
    std::cout << "  Axis1 Fuzzy Torque: " << data.Axis1FuzzyTorque << std::endl;
    std::cout << "  Axis1 Fuzzy Position: " << data.Axis1FuzzyPosition << std::endl;
    std::cout << "  Axis1 Position P: " << data.Axis1PositionP << std::endl;
    std::cout << "  Axis1 Position I: " << data.Axis1PositionI << std::endl;
    std::cout << "  Axis1 Position D: " << data.Axis1PositionD << std::endl;
    std::cout << "  Axis1 Torque P: " << data.Axis1TorqueP << std::endl;
    std::cout << "  Axis1 Torque D: " << data.Axis1TorqueD << std::endl;
    std::cout << "  Axis1 Requested State: " << data.Axis1RequestedState << std::endl;
//    std::cout << "  Checksum MOSI: " << data.ChecksumMOSI << std::endl;


    std::cout << "TxPDO:" << std::endl;
    std::cout << "  Axis0 Absolute Position: " << data.Axis0AbsolutePosition << std::endl;
    std::cout << "  Axis0 Current: " << data.Axis0Current << std::endl;
    std::cout << "  Axis0 Motor Velocity: " << data.Axis0MotorVelocity << std::endl;
    std::cout << "  Axis0 ODrive Error: " << data.Axis0ODriveError << std::endl;
    std::cout << "  Axis0 Axis Error: " << data.Axis0AxisError << std::endl;
    std::cout << "  Axis0 Motor Error: " << data.Axis0MotorError << std::endl;
    std::cout << "  Axis0 Encoder Error: " << data.Axis0EncoderError << std::endl;
    std::cout << "  Axis0 Torque Sensor Error: " << data.Axis0TorqueSensorError << std::endl;
    std::cout << "  Axis0 Controller Error: " << data.Axis0ControllerError << std::endl;
    std::cout << "  Axis0 State: " << data.Axis0State << std::endl;
    std::cout << "  Axis0 ODrive Temperature: " << data.Axis0ODriveTemperature << std::endl;
    std::cout << "  Axis0 Motor Temperature: " << data.Axis0MotorTemperature << std::endl;
    std::cout << "  Axis0 Shadow Count: " << data.Axis0ShadowCount << std::endl;
    std::cout << "  Axis0 Torque: " << data.Axis0Torque << std::endl;
    std::cout << "  Axis1 Absolute Position: " << data.Axis1AbsolutePosition << std::endl;
    std::cout << "  Axis1 Current: " << data.Axis1Current << std::endl;
    std::cout << "  Axis1 Motor Velocity: " << data.Axis1MotorVelocity << std::endl;
    std::cout << "  Axis1 ODrive Error: " << data.Axis1ODriveError << std::endl;
    std::cout << "  Axis1 Axis Error: " << data.Axis1AxisError << std::endl;
    std::cout << "  Axis1 Motor Error: " << data.Axis1MotorError << std::endl;
    std::cout << "  Axis1 Encoder Error: " << data.Axis1EncoderError << std::endl;
    std::cout << "  Axis1 Torque Sensor Error: " << data.Axis1TorqueSensorError << std::endl;
    std::cout << "  Axis1 Controller Error: " << data.Axis1ControllerError << std::endl;
    std::cout << "  Axis1 State: " << data.Axis1State << std::endl;
    std::cout << "  Axis1 ODrive Temperature: " << data.Axis1ODriveTemperature << std::endl;
    std::cout << "  Axis1 Motor Temperature: " << data.Axis1MotorTemperature << std::endl;
    std::cout << "  Axis1 Shadow Count: " << data.Axis1ShadowCount << std::endl;
    std::cout << "  Axis1 Torque: " << data.Axis1Torque << std::endl;
    std::cout << "  AIE Absolute Position: " << data.AIEAbsolutePosition << std::endl;
    std::cout << "  Checksum MISO: " << data.ChecksumMISO << std::endl;
//    std::cout << "  Checksum MOSI Status: " << data.ChecksumMOSIStatus << std::endl;
}

int main()
{
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t *handle = pcap_open_offline("/Users/kimhernandezsalvador/Downloads/mdrive_7_tsu_2_joints_actuated.pcapng", errbuf);  // Open the .pcapng file
    if (handle == NULL)
    {
        std::cerr << "Couldn't open file: " << errbuf << std::endl;
        return 2;
    }

    struct pcap_pkthdr header;
    const u_char *packet;

    while ((packet = pcap_next(handle, &header)) != NULL)
    {
        // Parse the Ethernet header
        struct ether_header *eth_header = (struct ether_header *)packet;

        // Check if the packet is an EtherCAT frame (EtherType 0x88A4)
        if (ntohs(eth_header->ether_type) == 0x88A4)
        {
            ProcessDataMDrive data;

            std::cout << "Packet length: " << header.len << " bytes" << std::endl;

            // Extract process data from the EtherCAT frame
            if(header.len == PACKET_SIZE) {
                extract_data(packet, data);
            }
        }
    }

    // Close the pcap file
    pcap_close(handle);

    return 0;
}
